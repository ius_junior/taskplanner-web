﻿using System.Security.Principal;

namespace TaskPlanner.BasicAuthorization
{
    public interface IProvidePrincipal
    {
        IPrincipal CreatePrincipal(string username, string password);
    }
}