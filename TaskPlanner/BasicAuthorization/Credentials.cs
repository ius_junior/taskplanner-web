﻿namespace TaskPlanner.BasicAuthorization
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public Credentials(string usename, string password)
        {
            Username = usename;
            Password = password;
        }
    }
}