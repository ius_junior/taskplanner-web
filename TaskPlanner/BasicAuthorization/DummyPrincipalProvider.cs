﻿using System.Security.Principal;
using TaskPlanner.Repository;
using System.Linq;
using System.Security.Claims;
using TaskPlanner.Services;

namespace TaskPlanner.BasicAuthorization
{
    public class DummyPrincipalProvider : IProvidePrincipal
    {
        public IPrincipal CreatePrincipal(string email, string password)
        {
            var userRepository = new UserRepository();
            var hashService = new HashService();
            var user = userRepository.Get(u => u.Email== email.ToLower() && u.Password == hashService.GetHashString(password)).FirstOrDefault();
            if (user == null)
                return null;


            var identity = new ApiIdentity(user);
            IPrincipal principal = new GenericPrincipal(identity, new[] { "User" });
            return principal;
        }
    }
}