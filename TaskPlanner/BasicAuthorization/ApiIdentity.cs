﻿using System;
using System.Security.Principal;
using TaskPlanner.DataModels;


namespace TaskPlanner.BasicAuthorization
{
    public class ApiIdentity : IIdentity
    {
        public UserDataModel User { get; private set; }

        public ApiIdentity(UserDataModel user)
        {
            this.User = user;
        }

        public string Name
        {
            get { return this.User.Id.ToString(); }
        }

        public string AuthenticationType
        {
            get { return "Basic"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }
    }
}