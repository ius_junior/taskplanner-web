﻿$(document).ready(function () {
    $('#auth').validate({
        rules: {
            login: {
                required: true
            },

            pass: {
                required: true,
                minlength: 4,
                maxlength: 20,
            },
        },

        messages: {
            login: {
                required: "Это поле обязательно для заполнения"
            },

            pass: {
                required: "Это поле обязательно для заполнения",
                minlength: "Пароль должен быть минимум 4 символа",
                maxlength: "Пароль должен быть максимум 20 символов",
            },
        }
    });
});