﻿angular.module('TaskPlannerApp', ['ngCookies', 'ui.router', 'ui.bootstrap', 'angular-loading-bar', 'ngAnimate', 'angularFileUpload', 'mgcrea.ngStrap.select', 'angularCharts'])
    .config(function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
    })
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/login');
        //$locationProvider.$locationProvider.html5Mode(true);

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'Home/Authorization',
                controller: 'loginCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'Home/Registration',
                controller: 'registerCtrl'
            })
            .state('main', {
                abstract: true,
                url: '/main',
                templateUrl: 'Home/Main',
                controller: 'userCtrl'
            })
            .state('main.index', {
                url: '/index',
                templateUrl: 'Home/MainIndex',
                controller: 'teamCtrl'
            })
            .state('main.profile', {
                url: '/profile',
                templateUrl: 'Home/UserProfile',
                controller: 'userCtrl'
            })
            .state('main.teamStatistic', {
                url: '/team/{id}/statistic',
                templateUrl: 'Home/TeamStatistic',
                controller: 'teamStatisticCtrl'

            })
            .state('main.team', {
                url: '/team/{id}',
                templateUrl: 'Home/Team',
                controller: 'projectCtrl'
            })
            .state('main.statistic', {
                url: '/project/{id}/statistic',
                templateUrl: 'Home/Statistic',
                controller: 'statisticCtrl'
            })
            .state('main.project', {
                url: '/project/{id}',
                templateUrl: 'Home/Project',
                controller: 'taskCtrl'
            })
            .state('main.task', {
                url: '/task/{id}',
                templateUrl: 'Home/Task'
            })
            .state('main.profileEdit', {
                url: '/profile/edit',
                templateUrl: 'Home/EditProfile'
            })
            .state('confirmation', {
                url: '/confirmation/{code}',
                templateUrl: 'Home/Confirmation'
            }) 
    })
    .run(function (userFactory) {
        console.log("run app");
        userFactory.init();
    });