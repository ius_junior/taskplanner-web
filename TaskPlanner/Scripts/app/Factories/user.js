﻿angular.module('TaskPlannerApp')
    .factory('userFactory', function ($http, $cookieStore) {
        var users;
        var userService = {};

        userService.init = function (id) {
            console.log("init user service");

            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/users').success(function (data, status) {
                users = data;
                console.log(users);
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        }
        userService.users = function () {
            return users;
        };

        return userService;
    });