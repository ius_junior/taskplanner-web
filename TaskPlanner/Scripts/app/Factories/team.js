﻿angular.module('TaskPlannerApp')
    .factory('teamFactory', function ($http, $cookieStore) {
        var teams;
        var teamService = {};

        teamService.init = function () {
            console.log("init team service");
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/teams').success(function (data, status) {
                teams = data;
                console.log(teams);
            }).error(function (data, status) {
                if(status==401)
                    alert("Authorization error!");
            });
        }
        teamService.addTeam = function (item) {
            teams.push(item);
        };
        teamService.removeTeam = function (item) {
            var index = teams.indexOf(item);
            teams.splice(index, 1);
        };
        teamService.teams = function () {
            return teams;
        };

        return teamService;
});