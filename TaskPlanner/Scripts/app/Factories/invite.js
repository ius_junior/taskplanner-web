﻿angular.module('TaskPlannerApp')
    .factory('inviteFactory', function ($http, $cookieStore, teamFactory) {
        var invites;
        var inviteService = {};
        var team = teamFactory;

        inviteService.init = function () {
            console.log("init invite service");
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/invites').success(function (data, status) {
                invites = data;
            }).error(function (data, status) {
                if (status == 401)
                    console.log("Authorization error!");
            });
            console.log(invites);
        }
        inviteService.acceptInvite = function (index) {
            var data = JSON.stringify({
                TeamId: invites[index].TeamId
            });

            $http.post('/api/invites/accept', data).success(function (data, status) {
                console.log(data);
                team.addTeam({ Id: invites[index].TeamId, Name: invites[index].TeamName });
                invites.splice(index, 1);
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        };
        inviteService.declineInvite = function (index) {
            var data = JSON.stringify({
                TeamId: invites[index].TeamId
            });

            $http.post('/api/invites/decline', data).success(function (data, status) {
                console.log(data);
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                invites.splice(index, 1);
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        };
        inviteService.invites = function () {
            return invites;
        };

        return inviteService;
    });