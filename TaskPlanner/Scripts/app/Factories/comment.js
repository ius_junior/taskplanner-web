﻿angular.module('TaskPlannerApp')
    .factory('commentFactory', function ($http, $cookieStore, teamFactory) {
        var comments;
        var commentService = {};

        commentService.init = function (id) {
            console.log("init comment service");
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('api/tasks/' + id + '/comments').success(function (data, status) {
                comments = data;
                console.log(comments.length);
                console.log(data);
            }).error(function (data, status) {
                if (status == 401)
                    console.log("Authorization error!");
            });
        }

        commentService.addItem = function (item) {
            console.log("add item");
            comments.push(item);
        }

        commentService.comments = function () {
            return comments;
        };

        return commentService;
    });