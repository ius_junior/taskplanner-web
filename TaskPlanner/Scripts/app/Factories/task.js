﻿angular.module('TaskPlannerApp')
    .factory('taskFactory', function ($http, $cookieStore, $location) {
        var tasks;
        var taskService = {};

        taskService.init = function (id) {
            console.log("init task service");
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/projects/'+id+'/tasks').success(function (data, status) {
                tasks = data;
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
            console.log(tasks);
        }
        taskService.addItem = function (item) {
            tasks.push(item);
        };
        taskService.removeItem = function (item) {
            var index = tasks.indexOf(item);
            tasks.splice(index, 1);
        };
        taskService.tasks = function () {
            return tasks;
        };

        return taskService;
    });