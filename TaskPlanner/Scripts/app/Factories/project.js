﻿angular.module('TaskPlannerApp')
    .factory('projectFactory', function ($http, $cookieStore) {
        var projects;
        var teammates;
        var projectService = {};

        projectService.init = function (id) {
            console.log("init project service");

            projects = null;
            teammates = null;

            //Get projects
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/teams/' + id + '/projects').success(function (data, status) {
                projects = data;
                console.log(projects);
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
            //Get teammate
            $http.defaults.headers.common['Authorization'] = $cookieStore.get('auth');
            $http.get('/api/teams/' + id + '/users').success(function (data, status) {
                teammates = data;
                console.log(teammates);
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        }
        projectService.excludeTeammate = function (userIndex, teamId) {
            console.log(teammates[userIndex].Id);
            console.log(teamId);
            if (teammates[userIndex].RoleId == 0) {
                var data = JSON.stringify({
                    TeamId: teamId,
                    UserId: teammates[userIndex].Id
                });
                $http.post('/api/teams/exclude-user', data).success(function (data, status) {
                    $.growl({
                        type: 'success',
                        icon: 'glyphicon glyphicon-ok',
                        message: ' ' + data.Message
                    });
                    teammates.splice(userIndex, 1);
                }).error(function (data, status) {
                    $.growl({
                        type: 'success',
                        icon: 'glyphicon glyphicon-ok',
                        message: ' ' + data.Message
                    });
                });
            }
        };
        projectService.projects = function () {
            return projects;
        };
        projectService.teammates = function () {
            return teammates;
        };
        projectService.addItem = function (item) {
            projects.push(item);
        };

        return projectService;
    });