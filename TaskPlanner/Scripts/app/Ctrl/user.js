﻿angular.module('TaskPlannerApp')
    .controller('userCtrl', function ($scope, $http, $cookieStore, $upload, inviteFactory) {
        $scope.user = $cookieStore.get('currentUser');
        console.log($scope.user);
        $scope.invite = inviteFactory;
        inviteFactory.init();
        $scope.onFileSelect = function ($files) {
            //$files: an array of files selected, each file has name, size, and type.
            $upload.upload({
                url: 'api/users/avatar',
                file: $files[0],
                progress: function (e) { }
            }).then(function (data, status, headers, config) {
                // file is uploaded successfully
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                refleshInfo();
            });
        }
        $scope.saveChanges = function () {
            var data = JSON.stringify({
                Id: $scope.user.Id,
                Name: $scope.user.Name,
                Surname: $scope.user.Surname,
                Info: $scope.user.Info,
                Skype: $scope.user.Skype,
                Vk: $scope.user.Vk,
                Twitter: $scope.user.Twitter,
                Facebook: $scope.user.Facebook
            });
            $http.put('/api/users', data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                refleshInfo();
            }).error(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-remove',
                    message: ' ' + data.Message
                });
            });
        }
        var refleshInfo = function () {
            $http.get('/api/authorization').success(function (data, status) {
                $cookieStore.put('currentUser', data);
                $scope.user = data;
            }).error(function (data, status) {
                alert("Authorization error!");
            });
        }
    });