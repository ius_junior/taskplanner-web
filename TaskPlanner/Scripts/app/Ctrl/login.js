﻿angular.module('TaskPlannerApp')
    .controller('loginCtrl', function ($scope, $http, $window, $cookieStore) {
        $scope.login = "";
        $scope.password = "";

        $scope.init = function () {
            console.log("init login controller");
        };

        $scope.connect = function () {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + Base64.encode($scope.login + ':' + $scope.password);
            $http.get('/api/authorization').success(function (data, status) {
                $cookieStore.put('currentUser', data);
                $cookieStore.put('auth', 'Basic ' + Base64.encode($scope.login + ':' + $scope.password));
                $window.location.href = '#/main/index';
            }).error(function (data, status) {
                alert("Authorization error!");
            });
        };
    
        $scope.disconnect = function() {
            $cookieStore.remove('auth');
            $cookieStore.remove('currentUser');
            $window.location.href = '#/login';
        };
    });