﻿angular.module('TaskPlannerApp')
    .controller('registerCtrl', function ($scope, $http, $window, $cookieStore) {
        $scope.email = "";
        $scope.password = "";
        $scope.confirmPassword = "";
        $scope.name = "";
        $scope.surname = "";

        $scope.registration = function () {
            var data = JSON.stringify({Name: $scope.name, Surname: $scope.surname, Email: $scope.email, Password: $scope.password, ConfirmPassword: $scope.confirmPassword});
            $http.post('/api/users',data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $window.location.href = '#/login';
            }).error(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
            });
        }
    });