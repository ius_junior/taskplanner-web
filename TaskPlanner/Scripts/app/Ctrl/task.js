﻿angular.module('TaskPlannerApp')
    .controller('taskCtrl', function ($scope, $http, $cookieStore, $window, $modal, $location, $stateParams, $state, taskFactory, commentFactory) {
        $scope.comment = commentFactory;
        $scope.tasks;
        $scope.params = $stateParams;
        $scope.selectedTask = null;
        $scope.selectedTaskIndex = null;
        $scope.currentProject;
        $scope.teammates = null;
        $scope.commentMessage = "";
        $scope.user = $cookieStore.get('currentUser');
        $scope.window = -2;
        $scope.addTask = {};
        $scope.editTask = {};
        $scope.users = null;
        $scope.comments;

        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.init = function () {
            $http.get('/api/projects/' + $stateParams.id + '/tasks').success(function (data, status) {
                $scope.tasks = data;
                console.log($scope.tasks);
                //если передан параметр id задачи
                if ($location.search().taskId !== undefined) {
                    for (var i = 0; i < $scope.tasks.length; ++i) {
                        if ($scope.tasks[i].Id == $location.search().taskId) {
                            $scope.selectTask(i);
                        }
                    }
                } else {
                    $location.search('taskId', null);
                }
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
            
            $http.get('api/projects/' + $stateParams.id).success(function (data, status) {
                $scope.currentProject = data;
                console.log(data);

                $http.get('/api/teams/' + $scope.currentProject.TeamId + '/users').success(function (data, status) {
                    $scope.users = data;
                }).error(function (data, status) {
                    if (status == 401)
                        alert("Authorization error!");
                });
            }).error(function (data, status) {
                console.log(data);
            });
        }
        $scope.showAddWindow = function () {
            $scope.addTask = {};
            var d = new Date();
            d.setHours(1);
            d.setMinutes(0);
            $scope.addTask.Time = d;
            $scope.addTask.Deadline = new Date();
            $scope.addTask.User = $scope.users[0];

            $scope.closeWindow();
            $scope.window = 1;
        }
        $scope.showEditWindow = function () {
            $scope.editTask = {};
            $scope.editTask.Id = $scope.selectedTask.Id;
            $scope.editTask.Name = $scope.selectedTask.Name;
            $scope.editTask.Info = $scope.selectedTask.Info;


            var id = $scope.selectedTask.UserName;
            for (var i = 0; i < $scope.users.length; ++i) {
                if (id == $scope.users[i].UserName)
                    $scope.editTask.User = $scope.users[i];
            }

            $scope.editTask.Time = $scope.selectedTask.TimeRating;
            $scope.editTask.Deadline = $scope.selectedTask.Deadline;

            $scope.window = 2;
        }
        $scope.editTaskSave = function () {
            var data = JSON.stringify({
                Id: $scope.editTask.Id,
                Name: $scope.editTask.Name,
                Info: $scope.editTask.Info,
                Deadline: $scope.editTask.Deadline,
                TimeRating: "00:00:00.1234567"
            });
            $http.put('/api/tasks', data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $scope.tasks[$scope.selectedTaskIndex].Name = $scope.editTask.Name;
                $scope.tasks[$scope.selectedTaskIndex].Info = $scope.editTask.Info;
                $scope.tasks[$scope.selectedTaskIndex].Deadline = $scope.editTask.Deadline;

                $scope.closeWindow();
            }).error(function (data, status) {
                $.growl({
                    type: 'warning',
                    icon: 'glyphicon glyphicon-cancel',
                    message: ' ' + data.Message
                });
            });
        }
        $scope.selectTask = function (index) {
            $scope.comments = [];
            $scope.selectedTaskIndex = index;
            $scope.selectedTask = $scope.tasks[index];
            $location.search('taskId', $scope.selectedTask.Id);
            $scope.window = 0;

            $http.get('api/tasks/' + $scope.selectedTask.Id + '/comments').success(function (data, status) {
                $scope.comments = data;
                console.log(data);
            }).error(function (data, status) {
                if (status == 401)
                    console.log("Authorization error!");
            });
        }
        $scope.closeWindow = function () {
            $scope.window = 0;
            $scope.selectedTaskIndex = null;
            $scope.selectedTask = null;
        }

        $scope.completeTask = function () {
            var data = JSON.stringify({
                Id: $scope.selectedTask.Id
            });
            $http.post('/api/tasks/complete', data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $scope.selectedTask.StatusByteString = "3";
                $scope.tasks[$scope.selectedTaskIndex].StatusByteString = "3";
            }).error(function (data, status) {
                $.growl({
                    type: 'warning',
                    icon: 'glyphicon glyphicon-cancel',
                    message: ' ' + data.Message
                });
            });
        }

        $scope.incompleteTask = function () {
            var data = JSON.stringify({
                Id: $scope.selectedTask.Id
            });
            $http.post('/api/tasks/decomplete', data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $scope.selectedTask.StatusByteString = "2";
                $scope.tasks[$scope.selectedTaskIndex].StatusByteString = "2";
            }).error(function (data, status) {
                $.growl({
                    type: 'warning',
                    icon: 'glyphicon glyphicon-cancel',
                    message: ' ' + data.Message
                });
            });
        }

        $scope.createTask = function () {
            var d = $scope.addTask.Time;
            var data = JSON.stringify({
                UserId: $scope.addTask.User.Id,
                ProjectId: $stateParams.id,
                Name: $scope.addTask.Name,
                Info: $scope.addTask.Info,
                Deadline: $scope.addTask.Deadline,
                TimeRating: d.getHours()+':'+d.getMinutes()+':00'
            });

            $http.post('/api/tasks', data).success(function (data, status) {
                var task = {
                    Id: data.Id,
                    Name: $scope.addTask.Name,
                    Avatar: $scope.addTask.User.Avatar,
                    Info: $scope.addTask.Info,
                    Deadline: $scope.addTask.Deadline,
                    TimeRating: d.getHours() + ':' + d.getMinutes() + ':00'
                };
                if ($scope.tasks.length == 0)
                    $scope.tasks = [];
                $scope.tasks.push(task);

                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $scope.closeWindow();
            }).error(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-cancel',
                    message: ' ' + data.Message
                });
            });
        }

        $scope.addComment = function () {
            var data = JSON.stringify({
                TaskId: $scope.selectedTask.Id,
                Message: $scope.commentMessage
            });
            $http.post('/api/comments', data).success(function (data, status) {
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                if($scope.comments.length==0)
                    $scope.comments = [];
                $scope.comments.push({ Message: $scope.commentMessage, Date: Date.now(), UserName: $scope.user.Name, UserSurname: $scope.user.Surname, UserAvatar: $scope.user.Avatar });
                $scope.commentMessage = "";
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        }

        $scope.editProject = function () {
            var model = $scope.currentProject;
            $modal.open({
                templateUrl: 'myModalContentEdit.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, $stateParams) {
                    $scope.projectName = model.Name;
                    $scope.projectInfo = model.Info;
                    $scope.submit = function () {
                        var data = JSON.stringify({
                            ProjectId: $stateParams.id,
                            Name: $scope.projectName,
                            Info: $scope.projectInfo
                        });
                        $http.put('/api/projects', data).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            model.Name = $scope.projectName;
                            model.Info = $scope.projectInfo;
                        }).error(function (data, status) {
                            $.growl({
                                type: 'warning',
                                icon: 'glyphicon glyphicon-cancel',
                                message: ' ' + data.Message
                            });
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.deleteProject = function () {
            var teamId = $scope.currentProject.TeamId;
            $modal.open({
                templateUrl: 'myDeleteModal.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance) {
                    $scope.submit = function () {
                        var dataModel = JSON.stringify({
                            ProjectId: $stateParams.id
                        });
                        $http.delete('/api/projects/', {
                            data: dataModel,
                            headers: {
                                'Content-type': 'application/json'
                            }
                        }).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            $window.location.href = '#/main/team/' + teamId;
                        }).error(function (data, status) {
                            $.growl({
                                type: 'warning',
                                icon: 'glyphicon glyphicon-cancel',
                                message: ' ' + data.Message
                            });
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.deleteTask = function () {
            var taskIndex = $scope.selectedTaskIndex;
            var taskId = $scope.selectedTask.Id;
            var tasks = $scope.tasks;
            var close = $scope.closeWindow;
            $modal.open({
                templateUrl: 'myDeleteModalTask.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance) {
                    $scope.submit = function () {
                        var dataModel = JSON.stringify({
                            Id: taskId
                        });
                        $http.delete('/api/tasks/', {
                            data: dataModel,
                            headers: {
                                'Content-type': 'application/json'
                            }
                        }).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            tasks.splice(taskIndex, 1);
                            close();
                        }).error(function (data, status) {
                            $.growl({
                                type: 'warning',
                                icon: 'glyphicon glyphicon-cancel',
                                message: ' ' + data.Message
                            });
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
    });