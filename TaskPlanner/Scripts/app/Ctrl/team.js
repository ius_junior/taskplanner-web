﻿angular.module('TaskPlannerApp')
    .controller('teamCtrl', function ($scope, $http, $cookieStore, $modal, $stateParams, $window, teamFactory) {
        $scope.team = teamFactory;
        $scope.params = $stateParams;
        console.log($stateParams);
        $scope.tasks = [];
        $scope.init = function () {
            teamFactory.init();
            console.log($scope.team.teams());

            $http.get('/api/tasks').success(function (data, status) {
                $scope.tasks = data;
                console.log($scope.tasks);
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        }
        $scope.createTeam = function () {
            $modal.open({
                templateUrl: 'myModalContent.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, teamFactory) {
                    var team = teamFactory;
                    $scope.teamName = "";
                    $scope.submit = function () {
                        var data = JSON.stringify({
                            TeamName: $scope.teamName
                        });
                        $http.post('/api/teams', data).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            team.addTeam({ Id: data.Id, Name: $scope.teamName});
                        }).error(function (data, status) {
                            $.growl({
                                type: 'warning',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
    });