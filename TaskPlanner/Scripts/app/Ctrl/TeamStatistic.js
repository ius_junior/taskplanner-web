﻿angular.module('TaskPlannerApp')
    .controller('teamStatisticCtrl', function ($scope, $http, $window, $cookieStore, $stateParams) {
        
        $scope.config = {
            title: 'Products',
            tooltips: true,
            labels: false,
            mouseover: function () { },
            mouseout: function () { },
            click: function () { },
            legend: {
                display: true,
                //could be 'left, right'
                position: 'right'
            }
        };
        $scope.init = function () {
            $http.get('api/teams/' + $stateParams.id + '/statistic').success(function (data, status) {
                console.log(data);
                console.log($scope.data);
                $scope.data = {
                    series: ["Выполненные","Невыполненные"],
                    data: [{
                        x: data[0].ProjectName,
                        y: [data[0].CompletedTasks,data[0].IncompletedTasks]
                    }]
                };
                console.log($scope.data);
                for (var i = 1; i < data.length; ++i) {
                    //$scope.data.series[$scope.data.series.length] = data[i].ProjectName;
                    $scope.data.data[$scope.data.data.length] = {
                        x: data[i].ProjectName,
                        y: [data[i].CompletedTasks,data[i].IncompletedTasks]
                        }
                    }
                console.log($scope.data);
            }).error(function (datas, status) {
                alert("Authorization error!");
            });
        }
        
        
    });