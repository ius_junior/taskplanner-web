﻿angular.module('TaskPlannerApp')
    .controller('projectCtrl', function ($scope, $http, $cookieStore, $stateParams, $modal, $window, projectFactory, userFactory) {
        $scope.project = projectFactory;
        $scope.user = userFactory;
        $scope.selectedUser = "";
        $scope.params = $stateParams;
        $scope.currentTeam = null;

        $scope.init = function () {
            projectFactory.init($stateParams.id);

            $http.get('api/teams/' + $stateParams.id).success(function (data, status) {
                $scope.currentTeam = data;
                console.log(data);
            }).error(function (data, status) {
                console.log(data);
            });
        }
        $scope.sendInvite = function () {
            if ($scope.selectedUser.Id == undefined) {
                $.growl({
                    type: 'warning',
                    icon: 'glyphicon glyphicon-remove',
                    message: ' Тут нужно написать валидацию!'
                });
                return;
            }
            var data = JSON.stringify({
                TeamId: $stateParams.id,
                UserId: $scope.selectedUser.Id
            });
            $http.post('api/teams/invite-user', data).success(function (data, status) {
                console.log(data);
                console.log(status);
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
            }).error(function (data, status) {
                console.log(data);
                console.log(status);
                $.growl({
                    type: 'warning',
                    icon: 'glyphicon glyphicon-remove',
                    message: ' ' + data.Message
                });
            });
        }

        $scope.leaveTeam = function () {
            var data = JSON.stringify({
                Id: $stateParams.id
            });
            $http.post('/api/teams/leave', data).success(function (data, status) {
                console.log(data);
                $.growl({
                    type: 'success',
                    icon: 'glyphicon glyphicon-ok',
                    message: ' ' + data.Message
                });
                $window.location.href = '#/main/index';
            }).error(function (data, status) {
                if (status == 401)
                    alert("Authorization error!");
            });
        }
        $scope.createProject = function () {
            $modal.open({
                templateUrl: 'myModalContent.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, projectFactory) {
                    var project = projectFactory;
                    $scope.projectName = "";
                    $scope.projectInfo = "";
                    $scope.submit = function () {
                        var data = JSON.stringify({
                            TeamId: $stateParams.id,
                            Name: $scope.projectName,
                            Info: $scope.projectInfo
                        });
                        console.log(data);
                        $http.post('/api/projects', data).success(function (data, status) {
                            console.log("Проект создан!");
                            console.log(data);
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' '+data.Message
                            });
                            project.addItem({ ProjectId: data.Id, Name: $scope.teamName, TeamId: $stateParams.Id, Name: $scope.projectName, Info: $scope.projectInfo });
                        }).error(function (data, status) {
                            if (status == 401)
                                alert("Authorization error!");
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.createProject = function () {
            $modal.open({
                templateUrl: 'myModalContent.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, projectFactory) {
                    var project = projectFactory;
                    $scope.projectName = "";
                    $scope.projectInfo = "";
                    $scope.submit = function () {
                        var data = JSON.stringify({
                            TeamId: $stateParams.id,
                            Name: $scope.projectName,
                            Info: $scope.projectInfo
                        });
                        console.log(data);
                        $http.post('/api/projects', data).success(function (data, status) {
                            console.log("Проект создан!");
                            console.log(data);
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' '+data.Message
                            });
                            project.addItem({ ProjectId: data.Id, Name: $scope.teamName, TeamId: $stateParams.Id, Name: $scope.projectName, Info: $scope.projectInfo });
                        }).error(function (data, status) {
                            if (status == 401)
                                alert("Authorization error!");
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.editTeam = function () {
            var model = $scope.currentTeam;
            $modal.open({
                templateUrl: 'myModalContentEdit.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, $stateParams) {
                    $scope.teamName = model.Name;
                    $scope.submit = function () {
                        var data = JSON.stringify({
                            TeamId: $stateParams.id,
                            TeamName: $scope.teamName
                        });
                        $http.put('/api/teams', data).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            model.Name = $scope.teamName;
                        }).error(function (data, status) {
                            $.growl({
                                type: 'warning',
                                icon: 'glyphicon glyphicon-cancel',
                                message: ' ' + data.Message
                            });
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.deleteTeam = function () {
            $modal.open({
                templateUrl: 'myDeleteModalTask.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance) {
                    $scope.submit = function () {
                        var dataModel = JSON.stringify({
                            Id: $stateParams.id
                        });
                        $http.delete('/api/teams/', {
                            data: dataModel,
                            headers: {
                            'Content-type': 'application/json'
                        }}).success(function (data, status) {
                            $.growl({
                                type: 'success',
                                icon: 'glyphicon glyphicon-ok',
                                message: ' ' + data.Message
                            });
                            $window.location.href = '#/main/index';
                        }).error(function (data, status) {
                            if (status == 401)
                                alert("Authorization error!");
                        });
                        $modalInstance.dismiss('cancel');
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
    });