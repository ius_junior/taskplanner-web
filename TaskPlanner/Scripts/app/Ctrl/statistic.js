﻿angular.module('TaskPlannerApp')
    .controller('statisticCtrl', function ($scope, $http, $window, $cookieStore, $stateParams) {
        $scope.config = {
            tooltips: true,
            labels: false,
            mouseover: function () { },
            mouseout: function () { },
            click: function () { },
            legend: {
                display: true,
                //could be 'left, right'
                position: 'right'
            }
        };
        
        $scope.init = function () {
            $http.get('/api/projects/' + $stateParams.id + '/statistic').success(function (data, status) {
                console.log(data);
                $scope.data = {
                    data: [{
                        x: "Не выполненные ["+data.NotStarted+"]",
                        y: [data.NotStarted]
                    }, {
                        x: "Выполненные [" + data.Completed + "]",
                        y: [data.Completed]
                    }]
                };
            }).error(function (datas, status) {
                    alert("Authorization error!");
            });
        }        
    });