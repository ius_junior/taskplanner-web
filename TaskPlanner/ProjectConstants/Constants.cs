﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ProjectConstants
{
    public static class Constants
    {
        /// <summary>
        /// 0-User 1-Admin 2-Invited 3-Guest
        /// </summary>
        public enum Role { User = 0, Admin, Invited, Guest };
        public enum TaskStatus { NotStarted = 0, InProgress, Paused, Completed}
        /// <summary>
        /// 0-User 1-Admin 2-Invited 3-Guest
        /// </summary>
        public static readonly string[] RolesArray;
        public static readonly string[] TaskStatusArray;
        public static readonly string[] FileTypeArray = { "image/pjpeg", "image/x-png", "image/gif" };
        public static readonly string[] FileExtentions = { ".jpg", ".png", ".gif" };

        public const string ImageFolder = @"\content\images\avatars\";
        public const string DefaultAvatar = @"defaultavatar.jpg";
        public const long MaxFileLength = 1048576;
        public const string MailDomain = "taskplanner@outlook.com";
        public const string MailUserLogin = "taskplanner@outlook.com";
        public const string MailUserPassword = "AsdfAsdf";
        public const string MailServer = "smtp-mail.outlook.com";
        public const int Port = 25;
        static Constants()
        {
            RolesArray = new string[] { Resources.User, Resources.Admin, Resources.Invited, Resources.Guest };
            TaskStatusArray = new string[] { Resources.NotStarted, Resources.InProgress, Resources.Paused, Resources.Completed };
        }

    }
}