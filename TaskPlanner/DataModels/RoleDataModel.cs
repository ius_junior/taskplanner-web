﻿using TaskPlanner.EntityDataModel;

namespace TaskPlanner.DataModels
{
    public class RoleDataModel
    {
        public byte Id { get; set; }
        public string RoleName { get; set; }

        public RoleDataModel(byte id, string role)
        {
            Id = id;
            RoleName = role;
        }
        public RoleDataModel(Role role)
        {
            Id = role.Id;
            RoleName = role.RoleName;
        }
    }
}