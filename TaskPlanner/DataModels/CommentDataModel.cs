﻿using System;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.DataModels
{
    public class CommentDataModel
    {
        public Guid Id { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public CommentDataModel(Guid id, Guid taskId, Guid userId, string message, DateTime date)
        {
            Id = id;
            TaskId = taskId;
            UserId = userId;
            Message = message;
            Date = date;
        }
        public CommentDataModel(Comment comment)
        {
            Id = comment.Id;
            TaskId = comment.TaskId;
            UserId = comment.UserId;
            Message = comment.Message;
            Date = comment.Date;
        }
    }
}