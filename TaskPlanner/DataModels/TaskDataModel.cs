﻿using System;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ViewModels.TaskViewModels;

namespace TaskPlanner.DataModels
{
    public class TaskDataModel
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public byte Status { get; set; }
        public string Info { get; set; }
        public DateTime Created { get; set; }
        public TimeSpan Progress { get; set; }
        public DateTime? Deadline { get; set; }
        public TimeSpan TimeRating { get; set; }
        public DateTime? PlannedOn { get; set; }
        public Guid UserId { get; set; }
        public DateTime StartTime { get; set; }

        public TaskDataModel(EntityDataModel.Task task)
        {
            Created = task.Created;
            Deadline = task.Deadline;
            Id = task.Id;
            Info = task.Info;
            Name = task.Name;
            PlannedOn = task.PlannedOn;
            Progress = TimeSpan.FromSeconds(task.Progress);
            ProjectId = task.ProjectId;
            Status = task.Status;
            TimeRating = TimeSpan.FromSeconds(task.TimeRating);
            UserId = task.UserId;
            StartTime = task.StartTime;
        }

        public TaskDataModel(Guid id, Guid projectId, string name, byte status, string info, DateTime created, TimeSpan progress, DateTime? deadline, TimeSpan timeRating, DateTime? plannedOn, Guid userId, DateTime startTime)
        {
            Id = id;
            ProjectId = projectId;
            Name = name;
            Status = status;
            Info = info;
            Created = created;
            Progress = progress;
            Deadline = deadline;
            TimeRating = timeRating;
            PlannedOn = plannedOn;
            UserId = userId;
            StartTime =startTime;
        }
    }
}