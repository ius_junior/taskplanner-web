﻿using System;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.DataModels
{
    public class UserToTeamDataModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public byte Role { get; set; }
        public Guid TeamId { get; set; }
        public UserToTeamDataModel(Guid id, Guid userId, byte role, Guid teamId)
        {
            Id = id;
            UserId = userId;
            Role = role;
            TeamId = teamId;
        }
        public UserToTeamDataModel(UserToTeam userToTeam)
        {
            Id = userToTeam.Id;
            UserId = userToTeam.UserId;
            Role = userToTeam.Role;
            TeamId = userToTeam.TeamId;
        }
    }
}