﻿using System;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;

namespace TaskPlanner.DataModels
{
    public class UserDataModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        public DateTime Created { get; set; }
        public bool Confirmed { get; set; }
        public string Info { get; set; }
        public string Skype { get; set; }
        public string Vk { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public UserDataModel(Guid id, string email, string password, string name, string surname,string avatar,DateTime created,bool confirmed,string info=null,string skype=null, string vk=null,string twitter=null,string facebook=null){
            Id = id;
            Email = email;
            Password = password;
            Name = name;
            Surname = surname;
            Avatar = avatar;
            Created = created;
            Confirmed = confirmed;
            Info = info;
            Skype = skype;
            Vk = vk;
            Twitter = twitter;
            Facebook = facebook;

        }

        public UserDataModel(User user)
        {
            Id = user.Id;
            Email = user.Email;
            Password = user.Password;
            Name = user.Name;
            Surname = user.Surname;
            Avatar = user.Avatar;
            Created = user.Created;
            Confirmed = user.Confirmed;
            Info = user.Info;
            Skype = user.Skype;
            Vk = user.Vk;
            Twitter = user.Twitter;
            Facebook = user.Facebook;
        }
    }
}