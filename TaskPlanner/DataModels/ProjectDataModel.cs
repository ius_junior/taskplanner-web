﻿using System;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.DataModels
{
    public class ProjectDataModel
    {
        public Guid Id { get; set; }
        public Guid TeamId { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public DateTime Created { get; set; }
        public ProjectDataModel(Guid id, Guid teamId, string name, string info,DateTime createDate)
        {
            Id = id;
            TeamId = teamId;
            Name = name;
            Info = info;
            Created = createDate;
        }
        public ProjectDataModel(Project project)
        {
            Id = project.Id;
            TeamId = project.TeamId;
            Name = project.Name;
            Info = project.Info;
            Created = project.Created;
        }
    }
}