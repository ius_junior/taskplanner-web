﻿using System;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.DataModels
{
    public class TeamDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public TeamDataModel(Guid id, string name,DateTime created)
        {
            Id = id;
            Name = name;
            Created=created;
        }
        public TeamDataModel(Team team)
        {
            Id = team.Id;
            Name = team.Name;
            Created = team.Created;
        }
    }
}