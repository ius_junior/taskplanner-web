﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.TeamViewModels.Out
{
    public class UserTeamViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public byte RoleId { get; set; }
        
    }
}