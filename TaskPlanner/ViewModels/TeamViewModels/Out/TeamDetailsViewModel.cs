﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.TeamViewModels.Out
{
    public class TeamDetailsViewModel
    {
        public string Name { get; set; }
        public string Role { get; set; }
        public byte RoleId { get; set; }
        public DateTime Created { get; set; }
    }
}