﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.TeamViewModels.Out
{
    public class UsersInTeam
    {
        public string Role { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string TeamName { get; set; }
        public string Avatar { get; set; }
    }
}