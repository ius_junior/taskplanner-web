﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.TeamViewModels.Out
{
    public class TeamStatistic
    {
        public string ProjectName { get; set; }
        public int CompletedTasks { get; set; }
        public int IncompletedTasks { get; set; }

        public TeamStatistic(string projectName, int completedTasks, int incompletedTasks)
        {
            ProjectName = projectName;
            CompletedTasks = completedTasks;
            IncompletedTasks = incompletedTasks;
        }
    }
}