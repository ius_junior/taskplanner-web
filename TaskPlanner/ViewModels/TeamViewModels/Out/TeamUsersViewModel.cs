﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Web;
using System.Web.Hosting;
using TaskPlanner.DataModels;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModels.TeamViewModels.Out
{
    public class TeamUsersViewModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Role { get; set; }
        public Byte RoleId { get; set; }
        public string Avatar { get; set; }

        public TeamUsersViewModel(HttpRequestMessage request, Guid id, string email, string name, string surname, string role, byte roleId, string avatar)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Email = email;
            Role = Constants.RolesArray[roleId];
            RoleId = roleId;
            Avatar = ImageService.GetAvatar(request, avatar);
            
        }

    }
}