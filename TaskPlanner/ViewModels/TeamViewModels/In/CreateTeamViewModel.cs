﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.TeamViewModels.In
{
    public class CreateTeamViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(20, ErrorMessageResourceName = "TeamNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string TeamName { get; set; }
    }
}