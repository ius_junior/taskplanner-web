﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.TeamViewModels.In
{
    public class ExcludeUserViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid TeamId { get; set; }
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid UserId { get; set; }
    }
}