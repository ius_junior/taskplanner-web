﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.CommentViewModels.In
{
    public class AddCommentViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid TaskId { get; set; }

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(200, ErrorMessageResourceName = "CommentLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Message { get; set; }
    }
}