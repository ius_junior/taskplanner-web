﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.CommentViewModels.In
{
    public class DeleteCommentViewModel
    {
        [Required(ErrorMessageResourceName="EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid Id { get; set; }
    }
}