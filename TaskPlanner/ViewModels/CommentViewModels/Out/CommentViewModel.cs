﻿
using System;
using System.ComponentModel.DataAnnotations;
namespace TaskPlanner.ViewModels.CommentViewModels.Out
{
    public class CommentViewModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public string UserEmail { get; set; }
        [Required]
        public string UserName {get; set; }
        [Required]
        public string UserSurname { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        public System.DateTime Date { get; set; }
        public string UserAvatar { get; set; }
        public CommentViewModel(Guid id, Guid userId, string userEmail, string userName,string userSurname, string message, System.DateTime date,string userAvatar)
        {
            Id = id;
            UserId = userId;
            UserEmail = userEmail;
            UserName = userName;
            UserSurname = userSurname;
            Message = message;
            Date = date;
            UserAvatar = userAvatar;
        }
    }
}