﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModels.TaskViewModels.Out
{
    public class TaskInProjectViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public byte StatusByte { get; set; }
        public string Info { get; set; }
        public System.DateTime Created { get; set; }
        public TimeSpan Progress { get; set; }
        public System.DateTime? Deadline { get; set; }
        public TimeSpan TimeRating { get; set; }
        public System.DateTime? PlannedOn { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        public TimeSpan TimePass { get; set; }
        public string StatusByteString { get; set; }
        public TaskInProjectViewModel(Guid id,string name,byte statusByte,string info,DateTime created, TimeSpan progress,DateTime? deadline,TimeSpan timeRating,DateTime? plannedOn,Guid userId,string userName,string surname,string avatar,HttpRequestMessage request)
        {
            Id = id;
            Name = name;
            Status = Constants.TaskStatusArray[statusByte];
            StatusByte = statusByte;
            StatusByteString = statusByte.ToString();
            Info = info;
            Created = created;
            Progress = progress;
            Deadline = deadline;
            TimeRating = timeRating;
            PlannedOn = plannedOn;
            UserId = userId;
            UserName = userName;
            Surname = surname;
            Avatar = ImageService.GetAvatar(request, avatar);
            TimePass = timeRating - progress;
        }
    }
}