﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.TaskViewModels.In
{
    public class SheduleTaskViewModel
    {
        public Guid Id { get; set; }
        public DateTime PlannedOn { get; set; }
    }
}