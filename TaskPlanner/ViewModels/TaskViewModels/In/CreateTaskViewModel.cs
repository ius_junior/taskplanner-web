﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.TaskViewModels.In
{
    public class CreateTaskViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid UserId { get; set; }

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid ProjectId { get; set; }

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        [MaxLength(50, ErrorMessageResourceName = "TaskNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(120, ErrorMessageResourceName = "TaskInfoLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Info { get; set; }

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public System.DateTime Deadline { get; set; }

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public TimeSpan TimeRating { get; set; }
    }
}