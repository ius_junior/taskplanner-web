﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.TaskViewModels.In
{
    public class TaskIdViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid Id { get; set; }
    }
}