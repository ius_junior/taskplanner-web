﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModels.UserViewModels.Out
{
    public class UserDetailViewModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        public DateTime Created { get; set; }
        public string Info { get; set; }
        public string Skype { get; set; }
        public string Vk { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public UserDetailViewModel() { }
        public UserDetailViewModel(HttpRequestMessage request, Guid id, string email, string name, string surname, string avatar, DateTime created, string info, string skype, string vk, string twitter, string facebook)
        {
            Id = id;
            Email = email;
            Name = name;
            Surname = surname;
            Avatar = ImageService.GetAvatar(request, avatar);
            Created = created;
            Info = info;
            Skype = skype;
            Vk = vk;
            Twitter = twitter;
            Facebook = facebook;
        }
    }
}