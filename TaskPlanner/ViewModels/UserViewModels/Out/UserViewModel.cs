﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModels.UserViewModels.Out
{

    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        public DateTime Created { get; set; }
        public UserViewModel() { }
        public UserViewModel(HttpRequestMessage request, Guid id, string email, string name, string surname, string avatar,DateTime created)
        {
            Id = id;
            Email = email;
            Name = name;
            Surname = surname;
            Avatar = ImageService.GetAvatar(request, avatar);
            Created = created;
        }
    }
}