﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.UserViewModels.In
{
    public class ChangeUserViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid Id { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "UserNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "UserSurnameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Surname { get; set; }
        [MaxLength(500, ErrorMessageResourceName = "UserInfoLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Info { get; set; }
        [MaxLength(50, ErrorMessageResourceName = "SkypeLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Skype { get; set; }
        [MaxLength(50, ErrorMessageResourceName = "VkLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Vk { get; set; }
        [MaxLength(50, ErrorMessageResourceName = "TwitterLenghtError", ErrorMessageResourceType = typeof(Resources))]
        public string Twitter { get; set; }
        [MaxLength(50, ErrorMessageResourceName = "FacebookLengthError", ErrorMessageResourceType = typeof(Resources))]
        public string Facebook { get; set; }
       
    }
}