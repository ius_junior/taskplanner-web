﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.UserViewModels.In
{

    public class UserRegisterViewModel
    {

        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        [EmailAddress(ErrorMessage="", ErrorMessageResourceName = "EmailVarificationFail", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }
       
        [MaxLength(20, ErrorMessageResourceName = "UserNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "UserSurnameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Surname { get; set; }

        [MaxLength(20, ErrorMessageResourceName = "PasswordLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Password { get; set; }
        
        [Compare("Password", ErrorMessageResourceName ="PasswordNotRepeated",ErrorMessageResourceType=typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }
        public UserRegisterViewModel() { }
    }
}