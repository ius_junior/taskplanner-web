﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModels.InviteViewModels.Out
{
    public class InviteViewModel
    {
        public string AdminName { get; set; }
        public Guid TeamId { get; set; }
        public string TeamName { get; set; }
        public string Avatar { get; set; }
        public InviteViewModel(HttpRequestMessage request,string adminName,Guid teamId,string teamName,string avatar)
        {
            AdminName = adminName;
            TeamId = teamId;
            TeamName = teamName;
            Avatar = ImageService.GetAvatar(request, avatar);
        }
        public InviteViewModel() { }
    }
}