﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;

namespace TaskPlanner.ViewModels.ProjectViewModels.Out
{
    public class ProjectDetailViewModel
    {
        public Guid TeamId { get; set; }
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public DateTime Created { get; set; }
        public string Role { get; set; }
        public byte RoleId { get; set; }


        public ProjectDetailViewModel(Guid teamId,Guid projectId,string name,string info, DateTime created,byte role)
        {
            TeamId = teamId;
            ProjectId =projectId;
            Name = name;
            Info = info;
            Created = created;
            Role = ProjectConstants.Constants.RolesArray[role];
            RoleId = role;


        }

    }
    
}