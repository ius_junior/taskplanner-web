﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.ViewModels.ProjectViewModels.Out
{
    public class ProjectStatisticViewModel
    {
        public int Total { get; set; }
        public int InProgress { get; set; }
        public int Completed { get; set; }
        public int Paused { get; set; }
        public int NotStarted { get; set; }
    }
}