﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;

namespace TaskPlanner.ViewModels.ProjectViewModels.Out
{
    public class ProjectViewModel
    {
        public Guid ProjectId { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public DateTime Created { get; set; }

        public ProjectViewModel(Guid id,string name, string info,DateTime created)
        {
            ProjectId = id;
            Name = name;
            Info = info;
            Created = created;
        }
        public ProjectViewModel(ProjectDataModel model)
        {
            ProjectId = model.Id;
            Name = model.Name;
            Info = model.Info;
            Created = model.Created;
        }
    }
}