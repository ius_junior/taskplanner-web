﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.ProjectViewModels.In
{
    public class ProjectIdViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid ProjectId { get; set; }
    }
}