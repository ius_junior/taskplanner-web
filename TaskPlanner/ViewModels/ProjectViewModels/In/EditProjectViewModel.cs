﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.ProjectViewModels.In
{
    public class EditProjectViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid ProjectId { get; set; }


        [MaxLength(50, ErrorMessageResourceName = "ProjectNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(120, ErrorMessageResourceName = "ProjectInfoLengthError", ErrorMessageResourceType = typeof(Resources))]
      
        public string Info { get; set; }
    }
}