﻿using System;
using System.ComponentModel.DataAnnotations;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModels.ProjectViewModels.In
{
    public class CreateProjectViewModel
    {
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public Guid TeamId { get; set; }

        [MaxLength(50, ErrorMessageResourceName = "ProjectNameLengthError", ErrorMessageResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "EmptyField", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(120, ErrorMessageResourceName = "ProjectInfoLengthError", ErrorMessageResourceType = typeof(Resources))]
      
        public string Info { get; set; }
    }
}