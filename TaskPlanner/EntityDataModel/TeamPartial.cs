﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskPlanner.EntityDataModel
{
    public partial class Team
    {
        public Team(Guid id,String name,DateTime created)
        {
            Id = id;
            Name = name;
            Created = created;
        }
    }
}