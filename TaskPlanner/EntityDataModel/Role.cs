//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TaskPlanner.EntityDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role
    {
        public Role()
        {
            this.UserToTeams = new HashSet<UserToTeam>();
        }
    
        public byte Id { get; set; }
        public string RoleName { get; set; }
    
        public virtual ICollection<UserToTeam> UserToTeams { get; set; }
    }
}
