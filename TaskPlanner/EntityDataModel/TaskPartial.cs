﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;

namespace TaskPlanner.EntityDataModel
{
    public partial class Task
    {
       public Task(TaskDataModel task)
        {
                Created = task.Created;
                Deadline = task.Deadline;
                Id = task.Id;
                Info = task.Info;
                Name = task.Name;
                PlannedOn = task.PlannedOn;
                Progress = task.Progress.Seconds;
                ProjectId = task.ProjectId;
                Status = task.Status;
                TimeRating = task.TimeRating.Seconds;
                UserId = task.UserId;
                StartTime = task.StartTime;
        }
    }
}