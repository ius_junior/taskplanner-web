﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;

namespace TaskPlanner.EntityDataModel
{
    public partial class UserToTeam
    {
       public UserToTeam(UserToTeamDataModel userToTeamDataModel)
        {
            Id = userToTeamDataModel.Id;
            Role = userToTeamDataModel.Role;
            TeamId = userToTeamDataModel.TeamId;
            UserId = userToTeamDataModel.UserId;
        }
       public UserToTeam() { }
    }
}