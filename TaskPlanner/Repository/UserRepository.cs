﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class UserRepository : BaseRepository
    {
        public IEnumerable<UserDataModel> Get(Func<User, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();    
            }
        }

        public IEnumerable<UserDataModel> Get(PlannerBaseEntities context, Func<User, bool> func)
        {
            return context.Users.Where(func).Select(u => new UserDataModel(u));
        }

        public void Add(PlannerBaseEntities context, UserDataModel user)
        {
            context.Users.Add(new User()
            {
                Id = user.Id,
                Email = user.Email.ToLower(),
                Password = user.Password,
                Name = user.Name,
                Surname = user.Surname,
                Created=user.Created,
                Avatar=user.Avatar
                
            });
            context.SaveChanges();
        }

        public void Add(UserDataModel user)
        {
            using (var context = GetContext())
            {
                Add(context, user);
            }
        }

        public bool Delete(PlannerBaseEntities context, Guid userId)
        {
            var user = context.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (user != null)
            {
                context.Users.Remove(user);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Delete(Guid userId)
        {
            using (var context = GetContext())
            {
                return Delete(context, userId);
            }
        }

        public bool Update(PlannerBaseEntities context, UserDataModel user)
        {
            var localUser = context.Users.Find(user.Id);
            if (localUser != null)
            {
                localUser.Email = user.Email.ToLower();
                localUser.Name = user.Name;
                localUser.Surname = user.Surname;
                localUser.Avatar = user.Avatar;
                localUser.Confirmed = user.Confirmed;
                localUser.Info = user.Info;
                localUser.Skype = user.Skype;
                localUser.Vk = user.Vk;
                localUser.Twitter = user.Twitter;
                localUser.Facebook = user.Facebook;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Update(UserDataModel user)
        {
            using (var context = GetContext())
            {
                return Update(context, user);
            }
        }
    }
}