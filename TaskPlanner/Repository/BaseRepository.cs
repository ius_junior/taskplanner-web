﻿using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class BaseRepository
    {
        protected PlannerBaseEntities GetContext()
        {
            return new PlannerBaseEntities();
        }
    }
}