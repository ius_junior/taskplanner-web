﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class ProjectRepository : BaseRepository
    {
        public IEnumerable<ProjectDataModel> Get(Func<Project, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context,func).ToArray();
            }
        }

        public IEnumerable<ProjectDataModel> Get(PlannerBaseEntities context, Func<Project, bool> func)
        {
            return context.Projects.Where(func).Select(p => new ProjectDataModel(p));
        }

        public bool Add(ProjectDataModel project)
        {
            using (var context = GetContext())
            {
                return Add(context, project);
            }
        }
        public bool Add(PlannerBaseEntities context, ProjectDataModel project)
        {
            try
            {
                context.Projects.Add(new Project()
                {
                    Id = project.Id,
                    Info = project.Info,
                    Name = project.Name,
                    TeamId = project.TeamId,
                    Created = project.Created
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Guid projectId)
        {
            using (var context = GetContext())
            {
                return Delete(context, projectId);
            }
        }

        public bool Delete(PlannerBaseEntities context, Guid projectId)
        {
                var project = context.Projects.Where(p => p.Id == projectId).FirstOrDefault();
                if (project != null)
                {
                    context.Projects.Remove(project);
                    context.SaveChanges();
                    return true;
                }
                return false;
        }

        public bool Update(ProjectDataModel project)
        {
            using (var context = GetContext())
            {
                return Update(context, project);
            }
        }
        public bool Update(PlannerBaseEntities context, ProjectDataModel project)
        {
           
                var localProject = context.Projects.Find(project.Id);
                if (localProject != null)
                {
                    localProject.Info = project.Info;
                    localProject.Name = project.Name;
                    context.SaveChanges();
                    return true;
                }
                return false;
        }
        public bool Update(PlannerBaseEntities context, Guid id,string info,string name)
        {
            var localProject = context.Projects.Find(id);
            if (localProject != null)
            {
                localProject.Info = info;
                localProject.Name = name;
                context.SaveChanges();
                return true;
            }
            return false;

        }
        public void Update(Guid id, string info, string name)
        {
            using (var context=GetContext())
            {
                Update(context, id, info, name);
            }

        }
    }
}