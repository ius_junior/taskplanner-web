﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class RoleRepository : BaseRepository
    {
        public IEnumerable<RoleDataModel> Get(Func<Role, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();
            }
        }

        public IEnumerable<RoleDataModel> Get(PlannerBaseEntities context, Func<Role, bool> func)
        {
            return context.Roles.Where(func).Select(r => new RoleDataModel(r));
        }

        public void Add(RoleDataModel role)
        {
            using (var context = GetContext())
            {
                Add(context, role);
            }
        }

        public void Add(PlannerBaseEntities context, RoleDataModel role)
        {
            context.Roles.Add(new Role()
            {
                Id = role.Id,
                RoleName = role.RoleName
            });
            context.SaveChanges();
        }

        public bool Delete(byte roleId)
        {
            using (var context = GetContext())
            {
                return Delete(context, roleId);
            }
        }

        public bool Delete(PlannerBaseEntities context, byte roleId)
        {
            var role = context.Roles.Where(r => r.Id == roleId).FirstOrDefault();
            if (role != null)
            {
                context.Roles.Remove(role);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Update(RoleDataModel role)
        {
            using (var context = GetContext())
            {
                return Update(context, role);
            }
        }

        public bool Update(PlannerBaseEntities context, RoleDataModel role)
        {
            var localRole = context.Roles.Find(role.Id);
            if (localRole != null)
            {
                localRole.Id = role.Id;
                localRole.RoleName = role.RoleName;
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}