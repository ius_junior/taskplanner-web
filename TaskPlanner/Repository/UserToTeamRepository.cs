﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class UserToTeamRepository : BaseRepository
    {
        public IEnumerable<UserToTeamDataModel> Get(Func<UserToTeam, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();
            }
        }

        public IEnumerable<UserToTeamDataModel> Get(PlannerBaseEntities context, Func<UserToTeam, bool> func)
        {
            return context.UserToTeams.Where(func).Select(u => new UserToTeamDataModel(u));
        }

        public bool Add(UserToTeamDataModel userToTeam)
        {
            using (var context = GetContext())
            {
                return Add(context, userToTeam);
            }
        }

        public bool Add(PlannerBaseEntities context, UserToTeamDataModel userToTeam)
        {
            try
            {
                context.UserToTeams.Add(new UserToTeam()
                {
                    Id = userToTeam.Id,
                    Role = userToTeam.Role,
                    TeamId = userToTeam.TeamId,
                    UserId = userToTeam.UserId
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Guid userToTeamId)
        {
            using (var context = GetContext())
            {
                return Delete(context, userToTeamId);
            }
        }
        public bool Delete(PlannerBaseEntities context, Guid userToTeamId)
        {
            var userToTeam = context.UserToTeams.Where(u => u.Id == userToTeamId).FirstOrDefault();
            if (userToTeam != null)
            {
                context.UserToTeams.Remove(userToTeam);
                context.SaveChanges();
                return true;
            }
            return false;
        }
        //public bool Delete(PlannerBaseEntities context, IEnumerable<UserToTeamDataModel> teamUsers)
        //{
        //    foreach (var teamUser in teamUsers)
        //    {
        //        context.UserToTeams.Remove(new UserToTeam(teamUser));
        //    }
        //    context.SaveChanges();
        //    return true;
        //}
        public bool Update(UserToTeamDataModel userToTeam)
        {
            using (var context = GetContext())
            {
                return Update(context, userToTeam);
            }
        }

        public bool Update(PlannerBaseEntities context, UserToTeamDataModel userToTeam)
        {
            var localUserToTeam = context.UserToTeams.Find(userToTeam.Id);
            if (localUserToTeam != null)
            {
                localUserToTeam.Role = userToTeam.Role;
                localUserToTeam.TeamId = userToTeam.TeamId;
                localUserToTeam.UserId = userToTeam.UserId;
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}