﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class CommentRepository : BaseRepository
    {
        public IEnumerable<CommentDataModel> Get(Func<Comment, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();
            }
        }

        public IEnumerable<CommentDataModel> Get(PlannerBaseEntities context, Func<Comment, bool> func)
        {
            return context.Comments.Where(func).Select(c => new CommentDataModel(c));
        }

        public bool Add(CommentDataModel comment)
        {
            using (var context = GetContext())
            {
                return Add(context, comment);
            }
        }

        public bool Add(PlannerBaseEntities context, CommentDataModel comment)
        {
            try
            {
                context.Comments.Add(new Comment()
                {
                    Id = comment.Id,
                    Date = comment.Date,
                    Message = comment.Message,
                    TaskId = comment.TaskId,
                    UserId = comment.UserId
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
                 
        }

        public bool Delete(Guid commentId)
        {
            using (var context = GetContext())
            {
                return Delete(context, commentId);
            }
        }
        public bool Delete(PlannerBaseEntities context, Guid commentId)
        {
            var comment = context.Comments.Where(c => c.Id == commentId).FirstOrDefault();
            if (comment != null)
            {
                context.Comments.Remove(comment);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Update(CommentDataModel comment)
        {
            using (var context = GetContext())
            {
                return Update(context, comment);
            }
        }
        public bool Update(PlannerBaseEntities context, CommentDataModel comment)
        {
                var localComment = context.Comments.Find(comment.Id);
                if (localComment != null)
                {
                    localComment.Date = comment.Date;
                    localComment.Message = comment.Message;
                    localComment.TaskId = comment.TaskId;
                    localComment.UserId = comment.UserId;
                    context.SaveChanges();
                    return true;
                }
                return false;
        }
    }
}