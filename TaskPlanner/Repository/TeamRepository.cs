﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class TeamRepository : BaseRepository
    {
        public IEnumerable<TeamDataModel> Get(Func<Team, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();
            }
        }

        public IEnumerable<TeamDataModel> Get(PlannerBaseEntities context, Func<Team, bool> func)
        {
            return context.Teams.Where(func).Select(u => new TeamDataModel(u));
        }

        public bool Add(TeamDataModel team)
        {
            using (var context = GetContext())
            {
                return Add(context, team);
            }
        }

        public bool Add(PlannerBaseEntities context, TeamDataModel team)
        {
            try
            {
                context.Teams.Add(new Team(team.Id,team.Name,team.Created));
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Guid teamId)
        {
            using (var context = GetContext())
            {
                return Delete(context, teamId);
            }
        }

        public bool Delete(PlannerBaseEntities context, Guid teamId)
        {
            var team = context.Teams.Where(u => u.Id == teamId).FirstOrDefault();
            if (team != null)
            {
                context.Teams.Remove(team);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Update(TeamDataModel team)
        {
            using (var context = GetContext())
            {
                return Update(context, team);
            }
        }

        public bool Update(PlannerBaseEntities context, TeamDataModel team)
        {
            var localUser = context.Teams.Find(team.Id);
            if (localUser != null)
            {
                localUser.Name = team.Name;
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}