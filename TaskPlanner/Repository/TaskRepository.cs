﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;

namespace TaskPlanner.Repository
{
    public class TaskRepository : BaseRepository
    {
        public IEnumerable<TaskDataModel> Get(Func<Task, bool> func)
        {
            using (var context = GetContext())
            {
                return Get(context, func).ToArray();
            }
        }

        public IEnumerable<TaskDataModel> Get(PlannerBaseEntities context, Func<Task, bool> func)
        {
            return context.Tasks.Where(func).Select(u => new TaskDataModel(u));
        }

        public bool Add(TaskDataModel task)
        {
            using (var context = GetContext())
            {
               return Add(context, task);
            }
        }
        public bool Add(PlannerBaseEntities context, TaskDataModel task)
        {
            try
            {
                context.Tasks.Add(new Task(task));
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(Guid taskId)
        {
            using (var context = GetContext())
            {
                return Delete(context, taskId);
            }
        }
        public bool Delete(PlannerBaseEntities context, Guid taskId)
        {
            var task = context.Tasks.Where(u => u.Id == taskId).FirstOrDefault();
            if (task != null)
            {
                context.Tasks.Remove(task);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Update(TaskDataModel task)
        {
            using (var context = GetContext())
            {
                return Update(context, task);
            }
        }
        public bool Update(PlannerBaseEntities context, TaskDataModel task)
        {
            var localTask = context.Tasks.Find(task.Id);
            if (localTask != null)
            {
                localTask.Deadline = task.Deadline;
                localTask.Info = task.Info;
                localTask.Name = task.Name;
                localTask.PlannedOn = task.PlannedOn;
                localTask.Progress = task.Progress.Seconds;
                localTask.ProjectId = task.ProjectId;
                localTask.Status = task.Status;
                localTask.TimeRating = task.TimeRating.Seconds;
                localTask.StartTime = task.StartTime;
                localTask.UserId = task.UserId;
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}