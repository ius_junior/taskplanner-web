﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;
using TaskPlanner.ViewModels;
using TaskPlanner.ViewModels.TaskViewModels;
using TaskPlanner.ViewModels.TaskViewModels.In;
using TaskPlanner.ViewModels.TaskViewModels.Out;

namespace TaskPlanner.Services
{
    public class TaskService
    {
        public IEnumerable<TaskViewModel> GetAllOwnTask(PlannerBaseEntities context, Guid userId, HttpRequestMessage request)
        {
            var taskRepository = new TaskRepository();
            var userToTeamRepository = new UserToTeamRepository();
            var projectRepository = new ProjectRepository();
            var userRepository = new UserRepository();
            return
                   from userToTeam in userToTeamRepository.Get(context, ut => ut.UserId == userId && (ut.Role == (byte)Constants.Role.User || ut.Role == (byte)Constants.Role.Admin))
                   join project in projectRepository.Get(context, p => true) on userToTeam.TeamId equals project.TeamId
                   join task in taskRepository.Get(context, t => t.UserId == userId) on project.Id equals task.ProjectId
                   join user in userRepository.Get(context, u => u.Id == userId) on task.UserId equals user.Id
                   select (new TaskViewModel(task.Id, task.Name, task.Status, task.Info, task.Created, task.Progress, task.Deadline, task.TimeRating, task.PlannedOn, user.Name, user.Surname, user.Avatar,project.Name,project.Id, userToTeam.TeamId,request));
        }
        public IEnumerable<TaskViewModel> GetAllOwnTask(Guid userId, HttpRequestMessage request)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetAllOwnTask(context, userId, request).ToArray();
            }
        }

        public IEnumerable<TaskInProjectViewModel> GetTasksByProject(Guid projectId, HttpRequestMessage request)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetTasksByProject(context, projectId, request).ToArray();
            }
        }
        public IEnumerable<TaskInProjectViewModel> GetTasksByProject(PlannerBaseEntities context, Guid projectId, HttpRequestMessage request)
        {
            var taskRepository = new TaskRepository();
            var userRepository = new UserRepository();
            return from task in taskRepository.Get(context, t => t.ProjectId == projectId)
                   join user in userRepository.Get(context, u => true) on task.UserId equals user.Id
                   select new TaskInProjectViewModel(task.Id, task.Name, task.Status, task.Info, task.Created, task.Progress, task.Deadline, task.TimeRating, task.PlannedOn,user.Id, user.Name, user.Surname, user.Avatar, request);
        }

        public IEnumerable<TaskViewModel> GetTasksByTeam(Guid teamId, HttpRequestMessage request)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetTasksByTeam(context, teamId, request);
            }
        }
        public IEnumerable<TaskViewModel> GetTasksByTeam(PlannerBaseEntities context, Guid teamId, HttpRequestMessage request)
        {
            var taskRepository = new TaskRepository();
            var projectRepository = new ProjectRepository();
            var userRepository = new UserRepository();

            return from project in projectRepository.Get(context, p => p.TeamId == teamId)
                   join task in taskRepository.Get(context, t => true) on project.Id equals task.ProjectId
                   join user in userRepository.Get(context, u => true) on task.UserId equals user.Id
                   select new TaskViewModel(task.Id, task.Name, task.Status, task.Info, task.Created, task.Progress, task.Deadline, task.TimeRating, task.PlannedOn, user.Name, user.Surname, user.Avatar,project.Name,project.Id,teamId, request);
        }
        public Guid AddTask(CreateTaskViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                return AddTask(context, model);
            }
        }
        public Guid AddTask(PlannerBaseEntities context, CreateTaskViewModel model)
        {
            var taskRepository = new TaskRepository();
            var taskGuid = Guid.NewGuid();
            if (taskRepository.Add(context,new TaskDataModel(taskGuid, model.ProjectId, model.Name, (byte)Constants.TaskStatus.NotStarted, model.Info, DateTime.Today, new TimeSpan(0), model.Deadline, model.TimeRating, null, model.UserId,DateTime.Now)))
                return taskGuid;
            return Guid.Empty;
        }
        public bool ChangeTask(ChangeTaskViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                return ChangeTask(context, model);
            }
        }
        public bool ChangeTask(PlannerBaseEntities context, ChangeTaskViewModel model)
        {
            var taskRepository = new TaskRepository();
            var task = taskRepository.Get(context, t => t.Id == model.Id).FirstOrDefault();
            if (taskRepository.Update(new TaskDataModel(task.Id, task.ProjectId, model.Name, task.Status, model.Info, task.Created, task.Progress, model.Deadline, model.TimeRating, task.PlannedOn, task.UserId,task.StartTime)))
                return true;
            return false;
        }
        public bool DeleteTask(PlannerBaseEntities context, Guid taskId)
        {
            var taskRepository = new TaskRepository();
            if (taskRepository.Delete(context,taskId))
                return true;
            return false;
        }
        public bool DeleteTask(Guid taskId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return DeleteTask(taskId);
            }
        }

        public TaskDetailViewModel GetTaskDetails(PlannerBaseEntities context, Guid taskId, HttpRequestMessage request)
        {
            var taskRepository = new TaskRepository();
            var task = taskRepository.Get(context, t => t.Id == taskId).FirstOrDefault();
            if (task == null) return null;
            var userRepository = new UserRepository();
            var user = userRepository.Get(context, u => u.Id == task.UserId).FirstOrDefault();
            if (user == null) return null;
            return new TaskDetailViewModel(task.Id, task.Name, task.Status, task.Info, task.Created, task.Progress, task.Deadline, task.TimeRating, task.PlannedOn, user.Name, user.Surname, user.Avatar, request);

        }
        public TaskDetailViewModel GetTaskDetails(Guid taskId, HttpRequestMessage request)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetTaskDetails(context, taskId, request);
            }
        }
        
    }
}