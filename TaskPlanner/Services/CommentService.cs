﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.Repository;
using TaskPlanner.ViewModels;
using TaskPlanner.ViewModels.CommentViewModels.Out;
using TaskPlanner.ViewModels.CommentViewModels.In;
using System.Net.Http;
namespace TaskPlanner.Services
{
    public class CommentService
    {
        public IEnumerable<CommentViewModel> GetCommentByTaskId(PlannerBaseEntities context,HttpRequestMessage request,Guid taskId)
        {
            var commentRepository = new CommentRepository();
            var userRepository = new UserRepository();
            return from comment in commentRepository.Get(context, c => c.TaskId == taskId)
                   join user in userRepository.Get(context, u => true) on comment.UserId equals user.Id
                   select new CommentViewModel(comment.Id,user.Id, user.Email, user.Name, user.Surname, comment.Message, comment.Date,ImageService.GetAvatar(request,user.Avatar));
        }
        public IEnumerable<CommentViewModel> GetCommentByTaskId(HttpRequestMessage request , Guid taskId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetCommentByTaskId(context,request,taskId);
            }
        }
        public bool AddCommentToTask(PlannerBaseEntities context, Guid userId, AddCommentViewModel model)
        {
            var commentRepository = new CommentRepository();
            if (commentRepository.Add(context, new CommentDataModel(Guid.NewGuid(), model.TaskId, userId, model.Message, DateTime.Now)))
                return true;
            return false;
        }
        public bool AddCommentToTask(Guid userId, AddCommentViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                return AddCommentToTask(context, userId, model);
            }
        }
        public bool ChangeOwnComment(PlannerBaseEntities context, ChangeCommentViewModel model)
        {
            var commentRepository = new CommentRepository();
            var comment=commentRepository.Get(context,c=>c.Id==model.Id).FirstOrDefault();
            if (comment != null)
                if (commentRepository.Update(context, new CommentDataModel(comment.Id, comment.TaskId, comment.UserId, model.Message, comment.Date)))
                    return true;
            return false;
        }
        public bool ChangeOwnComment(ChangeCommentViewModel model)
        {
            using (var context=new PlannerBaseEntities())
            {
                return ChangeOwnComment(context, model);
            }
        }
        public bool DeleteComment(PlannerBaseEntities context, Guid commentId)
        {
            var commentRepository = new CommentRepository();
            if (commentRepository.Delete(context, commentId))
                return true;
            return false;
        }
        public bool DeleteComment(Guid commentId)
        {
            using (var context=new PlannerBaseEntities())
            {
                return DeleteComment(context,commentId);
            }
        }
    }
}