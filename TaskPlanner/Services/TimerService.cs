﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;

namespace TaskPlanner.Services
{
    public class TimerService
    {        
        public bool StopTimer(PlannerBaseEntities context,Guid userId, Guid taskId)
        {
            var taskRepository = new TaskRepository();
            var task = taskRepository.Get(context, t => (t.Id == taskId&&t.UserId==userId&&t.Status==(byte)Constants.TaskStatus.InProgress)).FirstOrDefault();
            if (task == null) return false;
            task.Status = (byte)Constants.TaskStatus.Paused;
            task.Progress += (DateTime.Now - task.StartTime);
            taskRepository.Update(context, task);
            return true;
        }
    }
}