﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;
using TaskPlanner.ViewModels.InviteViewModels.Out;
using TaskPlanner.ViewModels.TeamViewModels;
using TaskPlanner.ViewModels.TeamViewModels.Out;

namespace TaskPlanner.Services
{
    public class InviteService
    {
        public IEnumerable<InviteViewModel> GetOwnInvites(PlannerBaseEntities context, HttpRequestMessage request,Guid userId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var teamRepository = new TeamRepository();
            var userRepository=new UserRepository();
            var admins = userToTeamRepository.Get(context, u => u.Role == (byte)Constants.Role.Admin).ToArray();

            //var inviteList = userToTeamRepository.Get(context, ut => ut.UserId == userId && ut.Role == (byte)Constants.Role.Invited).ToArray();
            //return inviteList.Select(s => new InviteViewModel(userRepository.Get(context, u => u.Id == admins.Where(a => a.TeamId == s.TeamId).FirstOrDefault().UserId).FirstOrDefault().Name,
            //                                                     s.TeamId,
            //                                                     teamRepository.Get(context, t => t.Id == s.TeamId).FirstOrDefault().Name)
            //                                                     ); 
            // 2 вариант
            var inviteList = userToTeamRepository.Get(context, ut => ut.UserId == userId && ut.Role == (byte)Constants.Role.Invited);

            return from invite in inviteList
                   join team in teamRepository.Get(context, t => true) on invite.TeamId equals team.Id
                   join userToTeam in userToTeamRepository.Get(context, ut => ut.Role == (byte)Constants.Role.Admin) on team.Id equals userToTeam.TeamId
                   join user in userRepository.Get(context, u => true) on userToTeam.UserId equals user.Id
                   select new InviteViewModel(request, user.Name, team.Id, team.Name, user.Avatar);     //если сможешь придумать лучше я не против.
        }

        public IEnumerable<InviteViewModel> GetOwnInvites(HttpRequestMessage request,Guid userId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetOwnInvites(context,request, userId).ToArray(); 
            }
        }

        public bool AcceptInvite(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var invite = userToTeamRepository.Get(context,i => i.UserId == userId && i.TeamId == teamId && i.Role == (byte)Constants.Role.Invited).FirstOrDefault();
            if (invite!=null)
            {
                invite.Role = (byte)Constants.Role.User;
                if (userToTeamRepository.Update(context, invite))
                    return true;
            }
            return false;
        }
        public bool AcceptInvite(Guid userId, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return AcceptInvite(context, userId, teamId); 
            }
        }
        public bool DeclineInvite(Guid userId, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return DeclineInvite(context, userId, teamId);
            }
        }
        public bool DeclineInvite(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var invite = userToTeamRepository.Get(context,i => i.UserId == userId && i.TeamId == teamId && i.Role == (byte)Constants.Role.Invited).FirstOrDefault();
            if (invite != null)
                if (userToTeamRepository.Delete(context, invite.Id))
                    return true;
            return false;
        }
    }
}