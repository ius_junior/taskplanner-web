﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Web;
using TaskPlanner.ProjectConstants;

namespace TaskPlanner.Services
{
    public class ImageService
    {


        [System.Runtime.InteropServices.DllImport("urlmon.dll",
        CharSet = System.Runtime.InteropServices.CharSet.Unicode,
        ExactSpelling = true, SetLastError = false)]
        static extern int FindMimeFromData(
            IntPtr pBC,

            [System.Runtime.InteropServices.MarshalAs(
                System.Runtime.InteropServices.UnmanagedType.LPWStr)]
             string pwzUrl,

            [System.Runtime.InteropServices.MarshalAs(
                System.Runtime.InteropServices.UnmanagedType.LPArray,
                ArraySubType = System.Runtime.InteropServices.UnmanagedType.I1,
                SizeParamIndex = 3)] 
             byte[] pBuffer,

             int cbSize,

             [System.Runtime.InteropServices.MarshalAs(
                 System.Runtime.InteropServices.UnmanagedType.LPWStr)]
             string pwzMimeProposed,

             int dwMimeFlags, out IntPtr ppwzMimeOut, int dwReserved);

        public static string GetMimeFromFile(byte[] fileArray)
        {
            IntPtr mimeout;

            byte[] buf = fileArray;
            int result = FindMimeFromData(IntPtr.Zero, "", buf, buf.Length, null, 0, out mimeout, 0);

            if (result != 0)
                throw System.Runtime.InteropServices.Marshal.GetExceptionForHR(result);

            string mime = System.Runtime.InteropServices.Marshal.PtrToStringUni(mimeout);
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(mimeout);
            return mime;
        }
        public static string GetAvatar(HttpRequestMessage request,string avatar)
        {
           return request.RequestUri.AbsoluteUri.Replace(request.RequestUri.PathAndQuery, String.Empty) + VirtualPathUtility.ToAbsolute(Constants.ImageFolder + avatar);
        }
    }
}
