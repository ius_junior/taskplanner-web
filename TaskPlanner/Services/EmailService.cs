﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using TaskPlanner.ProjectConstants;
using System.Text;

namespace TaskPlanner.Services
{
    public class EmailService
    {
        string _login;
        string _password;
        string _domain;
        public EmailService()
        {
            _login = Constants.MailUserLogin;
            _password = Constants.MailUserPassword;
            _domain = Constants.MailDomain;
        }


        public void SendMail(string recipient, string subject, string message)
        {
            SmtpClient client = new SmtpClient(Constants.MailServer);

            client.Port = Constants.Port;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(_login, _password);
            client.EnableSsl = true;
            client.Credentials = credentials;

            try
            {
                var mail = new MailMessage(_login.Trim(), recipient.Trim());
                mail.Subject = subject;
                mail.Body = message;

                mail.BodyEncoding = Encoding.UTF8;

                mail.IsBodyHtml = true;
                mail.SubjectEncoding = Encoding.UTF8;
                client.Send(mail);//MailAsync?
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}