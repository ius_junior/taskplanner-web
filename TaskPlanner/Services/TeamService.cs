﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;
using TaskPlanner.ViewModels.TeamViewModels;
using TaskPlanner.ViewModels.TeamViewModels.In;
using TaskPlanner.ViewModels.TeamViewModels.Out;

namespace TaskPlanner.Services
{
    public class TeamService
    {
        public IEnumerable<TeamDetailsViewModel> GetTeamById(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var teamRepository = new TeamRepository();
            var userToTeamRepository = new UserToTeamRepository();
            return (from t in teamRepository.Get(context, t => t.Id == teamId)
                    join u in userToTeamRepository.Get(context, u => u.TeamId == teamId && u.UserId == userId && (u.Role == (byte)Constants.Role.User || u.Role == (byte)Constants.Role.Admin)) on t.Id equals u.TeamId
                    select new TeamDetailsViewModel
                    {
                        Name = t.Name,
                        Role = Constants.RolesArray[u.Role],
                        RoleId = u.Role
                    });

        }
        public Guid CreateTeam(Guid userId, string teamName)
        {
            using (PlannerBaseEntities context = new PlannerBaseEntities())
            {
                return CreateTeam(context, userId, teamName);
            }
        }

        public Guid CreateTeam(PlannerBaseEntities context, Guid userId, string teamName)
        {
            var teamId = Guid.NewGuid();
            var teamRepository = new TeamRepository();
            if (!teamRepository.Add(context, new TeamDataModel(teamId, teamName, DateTime.Now))) return Guid.Empty;
            var userToTeamRepository = new UserToTeamRepository();
            if (!userToTeamRepository.Add(context, new UserToTeamDataModel(Guid.NewGuid(), userId, (byte)Constants.Role.Admin, teamId))) return Guid.Empty;
            return teamId;

        }
        public IEnumerable<UserTeamViewModel> GetTeamsByUserId(Guid userId)
        {

            using (var context = new PlannerBaseEntities())
            {
                return GetTeamsByUserId(context, userId).ToList();
            }
        }
        public IEnumerable<UserTeamViewModel> GetTeamsByUserId(PlannerBaseEntities context, Guid userId)
        {
            var teamRepository = new TeamRepository();
            var userToTeamRepository = new UserToTeamRepository();
            return (from t in teamRepository.Get(context, t => true)
                    join u in userToTeamRepository.Get(context, u => u.UserId == userId && (u.Role == (byte)Constants.Role.Admin || u.Role == (byte)Constants.Role.User)) on t.Id equals u.TeamId
                    select new UserTeamViewModel
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Role = Constants.RolesArray[u.Role],
                        RoleId = u.Role
                    });
        }
        public IEnumerable<TeamUsersViewModel> GetUsersByTeamId(HttpRequestMessage request, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetUsersByTeamId(request, context, teamId);
            }
        }
        public IEnumerable<TeamUsersViewModel> GetUsersByTeamId(HttpRequestMessage request, PlannerBaseEntities context, Guid teamId)
        {
            var userRepository = new UserRepository();
            var userToTeamRepository = new UserToTeamRepository();
            return (from u in userRepository.Get(context, t => true)
                    join ut in userToTeamRepository.Get(context, u => u.TeamId == teamId && (u.Role == (byte)Constants.Role.Admin || u.Role == (byte)Constants.Role.User)) on u.Id equals ut.UserId
                    select new TeamUsersViewModel(request, u.Id, u.Email, u.Name, u.Surname, Constants.RolesArray[ut.Role], ut.Role, u.Avatar));
        }
        public bool InviteUser(Guid userId, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return InviteUser(context, userId, teamId);
            }
        }
        public bool InviteUser(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var teamRepository = new TeamRepository();
            if (!userToTeamRepository.Get(context, ut => ut.UserId == userId && ut.TeamId == teamId).Any())
            {
                if (userToTeamRepository.Add(new UserToTeamDataModel(Guid.NewGuid(), userId, (byte)Constants.Role.Invited, teamId)))
                    return true;
            }
            return false;
        }
        public bool ExcludeUser(Guid userId, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return ExcludeUser(context, userId, teamId);
            }
        }
        public bool ExcludeUser(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var userInTeam = userToTeamRepository.Get(context, ut => ut.UserId == userId && ut.TeamId == teamId).FirstOrDefault();
            if (userInTeam != null)
            {
                if (userToTeamRepository.Delete(context, userInTeam.Id))
                    return true;
                return false;
            }
            return false;
        }
        public bool DeleteTeam(Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return DeleteTeam(context, teamId);
            }
        }
        public bool DeleteTeam(PlannerBaseEntities context, Guid teamId)
        {
            var teamRepository = new TeamRepository();
            if (teamRepository.Delete(context, teamId))
                return true;
            return false;
        }

        public bool EditTeam(EditTeamViewModel team)
        {
            using (var context = new PlannerBaseEntities())
            {
                return EditTeam(context, team);
            }
        }
        public bool EditTeam(PlannerBaseEntities context, EditTeamViewModel team)
        {
            var teamRepository = new TeamRepository();
            if (teamRepository.Update(context, new TeamDataModel(team.TeamId, team.TeamName, DateTime.Now)))
                return true;
            return false;
        }
        public IEnumerable<TeamStatistic> GetTaskStatistic(PlannerBaseEntities context, Guid teamId)
        {
            var teamRepository = new TeamRepository();
            var projectRepository=new ProjectRepository();
            var taskRepository=new TaskRepository();
            var team = teamRepository.Get(context, t => t.Id == teamId).FirstOrDefault();
            if (team == null) return null;
            return projectRepository.Get(context, p => p.TeamId == teamId).Select(p => new TeamStatistic(p.Name, taskRepository.Get(context, t => t.Status == (byte)Constants.TaskStatus.Completed && t.ProjectId == p.Id).Count(), taskRepository.Get(context, t => t.Status == (byte)Constants.TaskStatus.NotStarted && t.ProjectId == p.Id).Count()));
        }

    }
}