﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;
using TaskPlanner.ViewModels;
using TaskPlanner.ViewModels.ProjectViewModels;
using TaskPlanner.ViewModels.ProjectViewModels.In;
using TaskPlanner.ViewModels.ProjectViewModels.Out;

namespace TaskPlanner.Services
{
    public class ProjectService
    {
        public ProjectDetailViewModel GetProjectDetails(PlannerBaseEntities context, Guid userId, Guid projectId)
        {
           
            var projectRepository = new ProjectRepository();
            var userToteamRepository = new UserToTeamRepository();

            var project = projectRepository.Get(context, p => p.Id == projectId).FirstOrDefault();
            if (project != null)
            {
                var userToTeam = userToteamRepository.Get(context, ut => ut.UserId == userId && ut.TeamId == project.TeamId).FirstOrDefault();
                if (userToTeam != null)
                    return new ProjectDetailViewModel(project.TeamId, project.Id, project.Name, project.Info, project.Created, userToTeam.Role);
            }
            return null;


        }
        public IEnumerable<ProjectViewModel> GetProjectsInTeam(PlannerBaseEntities context, Guid teamId)
        {
            var projectRepository = new ProjectRepository();
            return projectRepository.Get(context, p => p.TeamId == teamId).Select(p => new ProjectViewModel(p));
        }
        public IEnumerable<ProjectViewModel> GetProjectsInTeam(Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetProjectsInTeam(teamId).ToArray();
            }
        }

        public Guid CreateNewProject(PlannerBaseEntities context, CreateProjectViewModel model)
        {
            var projectRepository = new ProjectRepository();
            var projectId = Guid.NewGuid();
            if (projectRepository.Add(context, new ProjectDataModel(projectId, model.TeamId, model.Name, model.Info, DateTime.Now)))
                return projectId;
            return Guid.Empty;
        }
        public Guid CreateNewProject(CreateProjectViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                return CreateNewProject(context, model);
            }
        }

        public bool EditProject(PlannerBaseEntities context, EditProjectViewModel model)
        {
            var projectRepository = new ProjectRepository();
            if (projectRepository.Update(context, model.ProjectId, model.Name, model.Info))
                return true;
            return false;                                                                     // мы вынуждены получать с формы teamId для формирования projectDataModel хотя teamId в update изменить мы не можем.
        }
        public bool EditProject(EditProjectViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                return EditProject(context, model);
            }
        }
        public bool DeleteProject(PlannerBaseEntities context, Guid projectId)
        {
            var projectRepository = new ProjectRepository();
            if (projectRepository.Delete(projectId))
                return true;
            return false;
        }
        public ProjectStatisticViewModel GetProjectStatistic(PlannerBaseEntities context, Guid projectId)
        {
            var taskRepository = new TaskRepository();
            var tasksInProject = taskRepository.Get(context, t => t.ProjectId == projectId);
            if (tasksInProject.Count() == 0) return null;
            return new ProjectStatisticViewModel()
            {
                Total = tasksInProject.Count(),
                NotStarted = tasksInProject.Where(t => t.Status == (byte)Constants.TaskStatus.NotStarted).Count(),
                Completed = tasksInProject.Where(t => t.Status == (byte)Constants.TaskStatus.Completed).Count()
            };
        }

    }
}