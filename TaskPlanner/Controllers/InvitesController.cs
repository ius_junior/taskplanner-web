﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.Properties;
using TaskPlanner.Services;
using TaskPlanner.ViewModels.InviteViewModels;
using TaskPlanner.ViewModels.InviteViewModels.In;

namespace TaskPlanner.Controllers
{
    public class InvitesController : ApiController
    {
        /// <summary>
        /// Get All Own Invites
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("api/invites")]
        public HttpResponseMessage GetAllInvites(HttpRequestMessage request)
        {
            var inviteService = new InviteService();
            var invites = inviteService.GetOwnInvites(request,((ApiIdentity)User.Identity).User.Id);  
            if (invites != null)
                return request.CreateResponse(HttpStatusCode.OK, invites);
            return request.CreateResponse(HttpStatusCode.NotFound, Resources.NoInvations);
        }
        /// <summary>
        /// AcceptInvite
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/invites/accept")]
        [HttpPost]

        public HttpResponseMessage AcceptInvite(HttpRequestMessage request,[FromBody]AcceptInviteViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                var inviteService = new InviteService();
                if (inviteService.AcceptInvite(((ApiIdentity)User.Identity).User.Id, model.TeamId))
                    return request.CreateResponse(HttpStatusCode.OK, Resources.SendInviteSuccess);
                return request.CreateResponse(HttpStatusCode.InternalServerError, Resources.SendInviteFail);
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage));
        }
        /// <summary>
        /// Decline invite
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/invites/decline")]
        [HttpPost]
        public HttpResponseMessage DeclineInvite(HttpRequestMessage request, DeclineInviteViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                var inviteService = new InviteService();
                if (inviteService.DeclineInvite(((ApiIdentity)User.Identity).User.Id, model.TeamId))
                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.DeclineInviteSuccess });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.DeclineInviteFail });
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
    }
}
