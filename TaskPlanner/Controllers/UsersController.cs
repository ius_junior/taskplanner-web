﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Properties;
using TaskPlanner.Repository;
using TaskPlanner.Services;
using TaskPlanner.UserViewModels.Out;
using TaskPlanner.Validaton;
using TaskPlanner.ViewModels.UserViewModels.In;
using TaskPlanner.ViewModels.UserViewModels.Out;

namespace TaskPlanner.Controllers
{
    public class UsersController : ApiController
    {
        /// <summary>
        /// Получить всех пользователей
        /// </summary>
        [Route("api/users")]
        [HttpGet]
        public HttpResponseMessage GetAllUsers(HttpRequestMessage request)
        {
            var userRepository = new UserRepository();
            var users = userRepository.Get(u => true).Select(u => new UserViewModel(request, u.Id, u.Email, u.Name, u.Surname, u.Avatar, u.Created));
            if (users != null)
                return request.CreateResponse(HttpStatusCode.OK, users);
            return request.CreateResponse(HttpStatusCode.NotFound, new { Message = Resources.EmptyUserList });


        }

        /// <summary>
        /// Получить пользователя по id
        /// </summary>
        [Authorize]
        [HttpGet]
        [Route("api/users/{UserId}")]
        public HttpResponseMessage GetUserById(HttpRequestMessage request, [FromUri] Guid UserId)
        {
            var userRepository = new UserRepository();
            var user = userRepository.Get(u => u.Id == UserId).Select(u => new UserDetailViewModel(request, u.Id, u.Email, u.Name, u.Surname, u.Avatar, u.Created, u.Info, u.Skype, u.Vk, u.Twitter, u.Facebook)).FirstOrDefault();
            if (user != null)
                return request.CreateResponse(HttpStatusCode.OK, user);
            return request.CreateResponse(HttpStatusCode.NotFound, new { Message = Resources.UserNotFound });
        }

        /// <summary>
        /// Отредактировать информацию пользователя (свою)+
        /// </summary>
        [Authorize]
        [Route("api/users")]
        [HttpPut]
        public HttpResponseMessage ChangeUser(HttpRequestMessage request, [FromBody]ChangeUserViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                if (model.Id.ToString() == User.Identity.Name)
                {
                    using (var context = new PlannerBaseEntities())
                    {
                        var checkDublicates = new CheckDublicates();
                        var userRepository = new UserRepository();
                        var current = ((ApiIdentity)User.Identity).User;
                        if (checkDublicates.ChangeUserEmailCheck(context, userRepository, current.Email, current.Id))
                        {
                            if (userRepository.Update(new UserDataModel(model.Id, current.Email, current.Password, model.Name, model.Surname, current.Avatar, current.Created, true, model.Info, model.Skype, model.Vk, model.Twitter, model.Facebook)))
                                return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.ChangeUserSuccess });
                        }
                        return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.DublicateEmailFound });
                    }
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Создание нового пользователя
        /// </summary>
        [Route("api/users")]
        [HttpPost]
        public HttpResponseMessage CreateUser(HttpRequestMessage request, [FromBody]UserRegisterViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var userRepository = new UserRepository();
                    var checkDublicates = new CheckDublicates();
                    if (checkDublicates.NewUserEmailCheck(context, userRepository, model.Email))
                    {
                        var userId = Guid.NewGuid();
                        var emailService = new EmailService();
                        var callbackUrl = request.RequestUri.AbsoluteUri.Replace(request.RequestUri.PathAndQuery, String.Empty) + "#/confirmation/" + new ShortGuid(userId);
                        var response = HttpContext.Current.Response;
                        //var html = ViewRenderer.RenderView("~/views/Home/EmailConfirmation.cshtml", new EmaiSendModel() { Link = callbackUrl });  //добавить ссылки на картинки
                        emailService.SendMail(model.Email, Resources.EmailConfirmationSubject, Resources.ConfirmEmailMessageBegin + callbackUrl + Resources.ConfirmEmailMessageEnd);   
                        var hashService = new HashService();
                        userRepository.Add(context, new UserDataModel(userId, model.Email, hashService.GetHashString(model.Password), model.Name, model.Surname, Constants.DefaultAvatar, DateTime.Now, false));
                        return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CreateUserSuccess });
                    }
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.DublicateEmailFound });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.SelectMany(s => s.Errors.Select(e => e.ErrorMessage)) });

        }
        /// <summary>
        /// Confirm user email
        /// </summary>
        /// <param name="request"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/users/confirm/{code}")]
        public HttpResponseMessage Confirm(HttpRequestMessage request, [FromUri]string code)
        {
            if (code != null)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var userRepository = new UserRepository();
                    Guid userId;
                    try
                    {
                        userId = new ShortGuid(code).Guid;
                    }
                    catch (Exception)
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                    var user = userRepository.Get(context, u => u.Id == userId && u.Confirmed == false).FirstOrDefault();
                    if (user != null)
                    {
                        user.Confirmed = true;
                        userRepository.Update(context, user);
                        return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.EmailConfirmationSuccess });
                    }

                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.EmailConfirmationFail });  //Можно сообщение об ошибке
        }

        /// <summary>
        /// Upload image
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("api/users/avatar")]
        public async Task<HttpResponseMessage> UploadImage(HttpRequestMessage Request)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            if (Request.Content.IsMimeMultipartContent())
            {
                try
                {
                    var currentUser = ((ApiIdentity)User.Identity).User;
                    await Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(new MultipartMemoryStreamProvider()).ContinueWith((task) =>
                    {
                        if (task.Status!=TaskStatus.Faulted)
                        {
                            MultipartMemoryStreamProvider provider = task.Result;
                            foreach (HttpContent content in provider.Contents)
                            {
                                Stream stream = content.ReadAsStreamAsync().Result;
                                if (stream.Length > Constants.MaxFileLength)
                                {
                                    result = Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.FileSizeInvalid });
                                    break;
                                }
                                string fileExtention = string.Empty;
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    stream.CopyTo(ms);
                                    var fileType = ImageService.GetMimeFromFile(ms.ToArray());
                                    for (int i = 0; i < Constants.FileTypeArray.Count(); i++)
                                    {
                                        if (fileType == Constants.FileTypeArray[i])
                                        {
                                            fileExtention = Constants.FileExtentions[i];
                                            break;
                                        }
                                    }
                                    if (fileExtention == string.Empty)
                                    {
                                        result = Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.FileTypeInvalid });
                                        break;
                                    }
                                }

                                Image image = Image.FromStream(stream);
                                String filePath = HostingEnvironment.MapPath(Constants.ImageFolder);
                                String fileName = Guid.NewGuid() + fileExtention;
                                String fullPath = Path.Combine(filePath, fileName);
                                image.Save(fullPath);
                                if (currentUser.Avatar != Constants.DefaultAvatar)
                                {
                                    System.IO.File.Delete(HostingEnvironment.MapPath(Constants.ImageFolder + currentUser.Avatar));
                                }
                                currentUser.Avatar = fileName;
                                new UserRepository().Update(currentUser);
                                result = Request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.AvatarChangeSuccess, NewUrl = ImageService.GetAvatar(Request, fileName) });
                                break;
                            }
                        }
                        else
                        result = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.InvalidRequestFormat });
                    });
                    return result;
                }
                catch (System.Exception)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.LoadImageFail });
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.LoadImageFail });
            }
        }
    }
}
