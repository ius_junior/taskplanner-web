﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Properties;
using TaskPlanner.Repository;
using TaskPlanner.Services;
using TaskPlanner.Validaton;
using TaskPlanner.ViewModels;
using TaskPlanner.ViewModels.ProjectViewModels;
using TaskPlanner.ViewModels.ProjectViewModels.In;

namespace TaskPlanner.Controllers
{
    [Authorize]
    public class ProjectsController : ApiController
    {
        /// <summary>
        /// Get project by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [Route("api/projects/{projectId}")]
        [HttpGet]
        public HttpResponseMessage GetProjectById(HttpRequestMessage request, [FromUri] Guid projectId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var roleValidation = new RoleValidation();
                var role = roleValidation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id, projectId);
                if (role == Constants.Role.Admin || role == Constants.Role.User)
                {
                    var projectService = new ProjectService();
                    var project = projectService.GetProjectDetails(context, ((ApiIdentity)User.Identity).User.Id, projectId);
                    if (project != null)
                        return request.CreateResponse(HttpStatusCode.OK, project);
                    return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.NoContentProjects });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
        /// <summary>
        /// Get projects in team
        /// </summary>
        [Route("api/teams/{teamId}/projects")]
        [HttpGet]
        public HttpResponseMessage GetProjetsInTeam(HttpRequestMessage request, [FromUri] Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var roleValidation = new RoleValidation();
                var role = roleValidation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, teamId);
                if (role == Constants.Role.Admin || role == Constants.Role.User)
                {
                    var projectService = new ProjectService();
                    var projects = projectService.GetProjectsInTeam(context, teamId);
                    if (projects != null)
                        return request.CreateResponse(HttpStatusCode.OK, projects.ToArray());
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.ProjectNotFuond });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
      

        /// <summary>
        /// Create new project	
        /// </summary>
        [Route("api/projects")]
        [HttpPost]
        public HttpResponseMessage CreateProject(HttpRequestMessage request, [FromBody] CreateProjectViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, model.TeamId);
                    if (role == Constants.Role.Admin)
                    {
                        var projectService = new ProjectService();
                        var projectId = projectService.CreateNewProject(context, model);
                        if(projectId!=Guid.Empty)
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CreateProjectSuccess, Id = projectId });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.CreateProjectFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Edit project
        /// </summary>
        [Route("api/projects")]
        [HttpPut]
        public HttpResponseMessage EditProject(HttpRequestMessage request, [FromBody] EditProjectViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id, model.ProjectId);
                    if (role == Constants.Role.Admin)
                    {
                        var projectsService = new ProjectService();
                        if (projectsService.EditProject(context, model))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.EditProjectSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.EditProjectFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, Resources.PermissionError);
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Delete project
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">Id</param>
        /// <returns></returns>
        [Route("api/projects")]
        [HttpDelete]
        public HttpResponseMessage DeleteProject(HttpRequestMessage request, [FromBody] ProjectIdViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id, model.ProjectId);
                    if (role == Constants.Role.Admin)
                    {
                        var projectService = new ProjectService();
                        if (projectService.DeleteProject(context, model.ProjectId))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.DeleteProjectSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.DeleteProjectFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
        [Route("api/projects/{projectId}/statistic")]
        [HttpGet]
        public HttpResponseMessage ProjectStatistic(HttpRequestMessage request, [FromUri] Guid projectId)  //попробовать передать null
        {
                using (var context = new PlannerBaseEntities()) 
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id,projectId);
                    if (role == Constants.Role.Admin || role==Constants.Role.User)
                    {
                        var projectService = new ProjectService();
                        var statistics = projectService.GetProjectStatistic(context, projectId);
                        if (statistics!=null)
                            return request.CreateResponse(HttpStatusCode.OK, statistics);
                        return request.CreateResponse(HttpStatusCode.InternalServerError,  new { Message = Resources.ProjectStatisticsError});
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
        }


    }
}
