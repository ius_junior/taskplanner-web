﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Properties;
using TaskPlanner.Repository;
using TaskPlanner.Services;
using TaskPlanner.Validaton;
using TaskPlanner.ViewModels.CommentViewModels.Out;
using TaskPlanner.ViewModels.CommentViewModels.In;

namespace TaskPlanner.Controllers
{
    /// <summary>
    /// Comments Controller
    /// </summary>
    [Authorize]
    public class CommentsController : ApiController
    {
        /// <summary>
        /// Get comments by taskId
        /// </summary>
        /// <param name="request"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [Route("api/tasks/{taskId}/comments")]
        [HttpGet]
        public HttpResponseMessage GetCommentsByTaskId(HttpRequestMessage request, [FromUri]Guid taskId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var validation = new RoleValidation();
                var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, taskId);
                if (role == Constants.Role.User || role == Constants.Role.Admin)
                {
                    var commentService = new CommentService();
                    var comments = commentService.GetCommentByTaskId(context,request, taskId).ToArray();
                    if (comments.Any())
                    {
                        return request.CreateResponse(HttpStatusCode.OK, comments);
                    }
                    return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.EmptyCommentsList });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
        /// <summary>
        /// Create new comment
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/comments")]
        [HttpPost]
        public HttpResponseMessage CreateComment(HttpRequestMessage request, [FromBody]AddCommentViewModel model)
        {
            if (model != null && ModelState.IsValid)
                using (var context = new PlannerBaseEntities())
                {
                    var validation = new RoleValidation();
                    var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, model.TaskId);
                    if (role == Constants.Role.User || role == Constants.Role.Admin)
                    {
                        var commentService = new CommentService();
                        if (commentService.AddCommentToTask(((ApiIdentity)User.Identity).User.Id, model))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CommentCreatedSuccess });
                        request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.CreateCommentFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
        /// <summary>
        /// Edit comment
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/comments")]
        [HttpPut]
        public HttpResponseMessage EditComment(HttpRequestMessage request, [FromBody]ChangeCommentViewModel model)
        {
            if (model != null && ModelState.IsValid)
                using (var context = new PlannerBaseEntities())
                {
                    var commentRepository = new CommentRepository();
                    var comment = commentRepository.Get(context, c => c.Id == model.Id).FirstOrDefault();
                    if (comment != null)
                    {
                        if (comment.UserId == ((ApiIdentity)User.Identity).User.Id)
                        {
                            var validation = new RoleValidation();
                            var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, comment.TaskId);
                            if (role == Constants.Role.User || role == Constants.Role.Admin)
                            {
                                var commentService = new CommentService();
                                if (commentService.ChangeOwnComment(context, model))
                                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CommentChangeSuccess });
                                request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.CommentChangeFail });
                            }
                            return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                        }
                    }

                }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/comments")]
        [HttpDelete]
        public HttpResponseMessage DeleteComment(HttpRequestMessage request, [FromBody]DeleteCommentViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var commentRepository = new CommentRepository();
                    var comment = commentRepository.Get(context, c => c.Id == model.Id).FirstOrDefault();
                    if (comment != null)
                    {
                        if (comment.UserId == ((ApiIdentity)User.Identity).User.Id)
                        {
                            var validation = new RoleValidation();
                            var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, comment.TaskId);
                            if (role == Constants.Role.User || role == Constants.Role.Admin)
                            {
                                var commentService = new CommentService();
                                if (commentService.DeleteComment(context, model.Id))
                                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CommentDeleteSuccess });
                                request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.CommentDeleteFail });
                            }
                            return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                        }
                    }
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.CommentNotFound });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
    }
}