﻿using System.Web.Mvc;

namespace TaskPlanner.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Authorization()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult Main()
        {
            return View();
        }

        public ActionResult Team()
        {
            return View();
        }

        public ActionResult Project()
        {
            return View();
        }

        public ActionResult Task()
        {
            return View();
        }

        public ActionResult MainIndex()
        {
            return View();
        }

        public ActionResult EditProfile()
        {
            return View();
        }

        public ActionResult Statistic()
        {
            return View();
        }
        public ActionResult Confirmation()
        {
            return View();
        }
        public ActionResult TeamStatistic()
        {
            return View();
        }
    }
}
