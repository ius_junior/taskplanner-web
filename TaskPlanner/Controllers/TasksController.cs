﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Properties;
using TaskPlanner.Repository;
using TaskPlanner.Services;
using TaskPlanner.Validaton;
using TaskPlanner.ViewModels.TaskViewModels;
using TaskPlanner.ViewModels.TaskViewModels.In;

namespace TaskPlanner.Controllers
{
    [Authorize]
    public class TasksController : ApiController
    {
        /// <summary>
        /// Получить инфо о задаче
        /// </summary>
        [Route("api/tasks/{taskId}")]
        [HttpGet]
        public HttpResponseMessage GetDetail(HttpRequestMessage request, [FromUri] Guid taskId)
        {
            using (var context = new PlannerBaseEntities())
            {

                var validation = new RoleValidation();
                var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, taskId);
                if (role == Constants.Role.User || role == Constants.Role.Admin)
                {
                    var taskService = new TaskService();
                    var task = taskService.GetTaskDetails(context, taskId, request);
                    if (task != null)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, task);
                    }
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskNotFound });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
        /// <summary>
        /// Получить все свои задачи
        /// </summary>
        [Route("api/tasks")]
        [HttpGet]
        public HttpResponseMessage GetTasks(HttpRequestMessage request)
        {
            var taskService = new TaskService();
            var tasks = taskService.GetAllOwnTask(((ApiIdentity)User.Identity).User.Id, request);
            if (!tasks.Any())
                return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.EmptyTaskList });
            return request.CreateResponse(HttpStatusCode.OK, tasks);
        }
        /// <summary>
        /// Get tasks in project
        /// </summary>
        /// <param name="request"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [Route("api/projects/{projectId}/tasks")]
        [HttpGet]
        public HttpResponseMessage GetTasksInProject(HttpRequestMessage request, [FromUri]Guid projectId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var validation = new RoleValidation();
                var role = validation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id, projectId);
                if (role == Constants.Role.User || role == Constants.Role.Admin)
                {
                    var taskService = new TaskService();
                    var tasks = taskService.GetTasksByProject(context, projectId, request);
                    if (tasks.Any())
                    {
                        return request.CreateResponse(HttpStatusCode.OK, tasks.ToArray());
                    }
                    return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.EmptyTaskList });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }

        /// <summary>
        /// CreateNewTask
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/tasks")]
        [HttpPost]
        public HttpResponseMessage CreateTask(HttpRequestMessage request, [FromBody] CreateTaskViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByProjectId(context, ((ApiIdentity)User.Identity).User.Id, model.ProjectId);
                    if (role == Constants.Role.Admin)
                    {
                        var userValidation = new UserValidation();
                        if (userValidation.IsUserInProject(context, model.UserId, model.ProjectId))
                        {
                            var taskService = new TaskService();
                            var taskId = taskService.AddTask(context, model);
                            if (taskId != Guid.Empty)
                                return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TaskCreatedSuccess, Id = taskId });
                            return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TaskCreatedFail });
                        }
                        return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.UserIsNotInTeam });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Change task
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/tasks")]
        [HttpPut]
        public HttpResponseMessage ChangeTask(HttpRequestMessage request, [FromBody] ChangeTaskViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {

                    var validation = new RoleValidation();
                    var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, model.Id);
                    if (role == Constants.Role.Admin)
                    {
                        var taskService = new TaskService();
                        if (taskService.ChangeTask(context, model))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.ChangeTaskSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.ChangeTaskFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, Resources.PermissionError);
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
        /// <summary>
        /// Delete task
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/tasks")]
        [HttpDelete]
        public HttpResponseMessage DeleteTask(HttpRequestMessage request, [FromBody] TaskIdViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var validation = new RoleValidation();
                var role = validation.GetRoleByTaskId(context, ((ApiIdentity)User.Identity).User.Id, model.Id);
                if (role == Constants.Role.Admin)
                {
                    var taskService = new TaskService();
                    if (taskService.DeleteTask(context, model.Id))
                        return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.DeleteTaskSuccess });
                    return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.DeleteTaskFail });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
        /// <summary>
        /// Start task timer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">Id</param>
        /// <returns></returns>
        [Route("api/tasks/timer/start")]
        [HttpPost]
        public HttpResponseMessage StartTimer(HttpRequestMessage request, [FromBody] TaskIdViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var taskRepository = new TaskRepository();
                var task = taskRepository.Get(context, t => (t.Id == model.Id && t.UserId == ((ApiIdentity)User.Identity).User.Id)).FirstOrDefault();
                if (task == null) 
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskIsNotYours });
                if (!(task.Status == (byte)Constants.TaskStatus.NotStarted||task.Status==(byte)Constants.TaskStatus.Paused))
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TimerAlreadyStarted });

                task.Status = (byte)Constants.TaskStatus.InProgress;
                task.StartTime = DateTime.Now;
                if (taskRepository.Update(context, task))
                return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TimerStarted });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TimerError });
            }
        }
        [Route("api/tasks/timer/stop")]
        [HttpPost]
        public HttpResponseMessage StopTimer(HttpRequestMessage request, [FromBody] TaskIdViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var taskRepository = new TaskRepository();
                var task = taskRepository.Get(context, t => (t.Id == model.Id && t.UserId == ((ApiIdentity)User.Identity).User.Id)).FirstOrDefault();
                if (task == null)
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskIsNotYours });
                if (task.Status != (byte)Constants.TaskStatus.InProgress)
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TimerNotStarted });

                task.Status = (byte)Constants.TaskStatus.Paused;
                task.Progress += (DateTime.Now - task.StartTime);
                if (taskRepository.Update(context, task))
                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TimerStopped });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TimerError });
            }
        }

        [Route("api/tasks/shedulate")]
        [HttpPost]
        public HttpResponseMessage SheduleTask(HttpRequestMessage request,[FromBody] SheduleTaskViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var taskRepository = new TaskRepository();
                var task = taskRepository.Get(context, t => (t.Id == model.Id && t.UserId == ((ApiIdentity)User.Identity).User.Id)).FirstOrDefault();
                if (task == null)
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskIsNotYours });
                task.PlannedOn = model.PlannedOn;

                if (taskRepository.Update(context, task))
                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TaskSheduleSuccess });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TaskSheduleFail });
            }
        }
        [Route("api/tasks/complete")]
        [HttpPost]
        public HttpResponseMessage CompleteTask(HttpRequestMessage request,[FromBody] TaskIdViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var taskRepository = new TaskRepository();
                var task = taskRepository.Get(context, t => (t.Id == model.Id && t.UserId == ((ApiIdentity)User.Identity).User.Id)).FirstOrDefault();
                if (task == null)
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskIsNotYours });
                task.Status = (byte)Constants.TaskStatus.Completed;

                if (taskRepository.Update(context, task))
                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TaskCompleteSuccess });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TaskCompleteFail });
            }
        }

        [Route("api/tasks/decomplete")]
        [HttpPost]
        public HttpResponseMessage DecompleteTask(HttpRequestMessage request, [FromBody] TaskIdViewModel model)
        {
            using (var context = new PlannerBaseEntities())
            {
                var taskRepository = new TaskRepository();
                var task = taskRepository.Get(context, t => (t.Id == model.Id && t.UserId == ((ApiIdentity)User.Identity).User.Id)).FirstOrDefault();
                if (task == null)
                    return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = Resources.TaskIsNotYours });
                task.Status = (byte)Constants.TaskStatus.NotStarted;

                if (taskRepository.Update(context, task))
                    return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.TaskIncompleteSuccess });
                return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TaskIncompleteFail });
            }
        }
    }
}
