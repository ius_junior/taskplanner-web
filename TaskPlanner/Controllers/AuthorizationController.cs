﻿using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.Repository;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.ViewModels.UserViewModels;
using TaskPlanner.ViewModels.UserViewModels.Out;

namespace TaskPlanner.Controllers
{
    /// <summary>
    /// Authorize user
    /// </summary>
    public class AuthorizationController : ApiController
    {
        [Authorize]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            var userRepository = new UserRepository(); 
            var currentUser = ((ApiIdentity)User.Identity).User;
            return request.CreateResponse(HttpStatusCode.OK,new UserDetailViewModel(request,currentUser.Id,currentUser.Email,currentUser.Name,currentUser.Surname,currentUser.Avatar,currentUser.Created,currentUser.Info,currentUser.Skype,currentUser.Vk,currentUser.Twitter,currentUser.Facebook));
        }
    }
}
