﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskPlanner.BasicAuthorization;
using TaskPlanner.DataModels;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Properties;
using TaskPlanner.Repository;
using TaskPlanner.Services;
using TaskPlanner.Validaton;
using TaskPlanner.ViewModels.TaskViewModels.In;
using TaskPlanner.ViewModels.TeamViewModels;
using TaskPlanner.ViewModels.TeamViewModels.In;

namespace TaskPlanner.Controllers
{
    public class TeamsController : ApiController
    {
        /// <summary>
        /// Get team info by teamId
        /// </summary>
        /// <param name="request"></param>
        /// <param name="teamId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("api/teams/{teamId}")]
        public HttpResponseMessage GetTeamInfo(HttpRequestMessage request, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var roleValidation = new RoleValidation();
                var role = roleValidation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, teamId);
                if (role == Constants.Role.User || role == Constants.Role.Admin)
                {
                    var teamService = new TeamService();
                    var team = teamService.GetTeamById(context, ((ApiIdentity)User.Identity).User.Id, teamId).FirstOrDefault();
                    if (team != null)
                        return request.CreateResponse(HttpStatusCode.OK, team);
                    return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.EmptyTeamInfo });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }

        /// <summary>
        /// Get all teams of current user
        /// </summary>
        [Authorize]
        [HttpGet]
        [Route("api/teams")]
        public HttpResponseMessage GetAllOwnTeams(HttpRequestMessage request)
        {
            var teamService = new TeamService();
            var teams = teamService.GetTeamsByUserId(((ApiIdentity)User.Identity).User.Id);
            if (teams.Any())
                return request.CreateResponse(HttpStatusCode.OK, teams);
            return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.EmptyTeamList });
        }

        /// <summary>
        /// Get users in team on teamId
        /// </summary>
        /// <param name="request"></param>
        /// <param name="teamId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("api/teams/{teamId}/users")]
        public HttpResponseMessage GetUsersInTeam(HttpRequestMessage request, [FromUri]Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                var roleValidation = new RoleValidation();
                var role = roleValidation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, teamId);
                if (role == Constants.Role.User || role == Constants.Role.Admin)
                {
                    var teamService = new TeamService();
                    var users = teamService.GetUsersByTeamId(request, context, teamId);
                    if (users != null)
                        return request.CreateResponse(HttpStatusCode.OK, users.ToArray());
                    return request.CreateResponse(HttpStatusCode.NoContent, new { Message = Resources.NoUsersInTeam });
                }
                return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
            }
        }

        /// <summary>
        /// Create new team
        /// </summary>
        /// <param name="request"></param>
        /// <param name="team"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("api/teams")]
        public HttpResponseMessage CreateTeam(HttpRequestMessage request, [FromBody]CreateTeamViewModel team)
        {
            if (team != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var teamService = new TeamService();
                    var teamId = teamService.CreateTeam(context, ((ApiIdentity)User.Identity).User.Id, team.TeamName);
                    if (teamId != Guid.Empty)
                        return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.CreateTeamSuccess, Id = teamId });
                    return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.CreateTeamFail });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Invite user in team
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/teams/invite-user")]
        [HttpPost]
        public HttpResponseMessage InviteUser(HttpRequestMessage request, [FromBody] InviteUserViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var validation = new RoleValidation();
                    var rights = validation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, model.TeamId);
                    if (rights == Constants.Role.Admin||rights==Constants.Role.User)
                    {
                        var teamService = new TeamService();
                        if (teamService.InviteUser(context, model.UserId, model.TeamId))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.SendInviteSuccess });
                        return request.CreateResponse(HttpStatusCode.Conflict, new { Message = Resources.UserAlreadyHasInvite });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Exclude user from team
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/teams/exclude-user")]
        [HttpPost]
        public HttpResponseMessage ExcludeUser(HttpRequestMessage request, [FromBody] ExcludeUserViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var validation = new RoleValidation();
                    var rights = validation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, model.TeamId);
                    if (rights == Constants.Role.Admin)
                    {
                        var teamService = new TeamService();
                        if (teamService.ExcludeUser(context, model.UserId, model.TeamId))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.ExcludeUserSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.ExcludeUserFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Delete team
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">Id</param>
        /// <returns></returns>
        [Authorize]
        [Route("api/teams")]
        [HttpDelete]
        public HttpResponseMessage DeleteTeam(HttpRequestMessage request, [FromBody] TeamIdViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var validation = new RoleValidation();
                    var rights = validation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, model.Id);
                    if (rights == Constants.Role.Admin)
                    {
                        var teamService = new TeamService();
                        if (teamService.DeleteTeam(context, model.Id))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.DeleteTeamSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.DeleteTeamFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Edit team
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        [Route("api/teams")]
        public HttpResponseMessage EditTeam(HttpRequestMessage request, [FromBody] EditTeamViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var validation = new RoleValidation();
                    var rights = validation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, model.TeamId);
                    if (rights == Constants.Role.Admin)
                    {
                        var teamService = new TeamService();
                        if (teamService.EditTeam(context, model))
                            return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.EditTeamSuccess });
                        return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.EditTeamFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }

        /// <summary>
        /// Leave project
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model">Id</param>
        /// <returns></returns>
        [Route("api/teams/leave")]
        [HttpPost]
        public HttpResponseMessage LeaveTeam(HttpRequestMessage request, [FromBody] TeamIdViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                using (var context = new PlannerBaseEntities())
                {
                    var roleValidation = new RoleValidation();
                    var role = roleValidation.GetRoleByTeamId(((ApiIdentity)User.Identity).User.Id, model.Id);
                    if (role != Constants.Role.Admin)
                    {
                        var userToTeamRepository = new UserToTeamRepository();
                        var UserToTeamFind = userToTeamRepository.Get(context, ut => (ut.UserId == ((ApiIdentity)User.Identity).User.Id) && ut.TeamId == model.Id).FirstOrDefault();
                        if (UserToTeamFind != null)
                        {
                            if (userToTeamRepository.Delete(context, UserToTeamFind.Id))
                                return request.CreateResponse(HttpStatusCode.OK, new { Message = Resources.LeaveTeamSuccess });
                        }
                        return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.LeaveTeamUserFail });
                    }
                    return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.LeaveTeamAdminFail });
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ModelState.Values.Select(s => s.Errors.FirstOrDefault().ErrorMessage) });
        }
        [Route("api/teams/{teamId}/statistic")]
        [HttpGet]
        public HttpResponseMessage TeamStatistic(HttpRequestMessage request, [FromUri] Guid teamId)  //попробовать передать null
        {
            using (var context = new PlannerBaseEntities())
            {
                var roleValidation = new RoleValidation();
                var role = roleValidation.GetRoleByTeamId(context, ((ApiIdentity)User.Identity).User.Id, teamId);
                if (role == Constants.Role.Admin || role == Constants.Role.User)
                {
                    var teamService = new TeamService();
                    var statistics = teamService.GetTaskStatistic(context, teamId);
                    if (statistics != null)
                        return request.CreateResponse(HttpStatusCode.OK, statistics.ToArray());
                    return request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = Resources.TeamStatisticError });
                }
                return request.CreateResponse(HttpStatusCode.Forbidden, new { Message = Resources.PermissionError });
            }
        }
    }
}
