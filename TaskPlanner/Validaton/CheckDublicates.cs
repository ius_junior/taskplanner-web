﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.EntityDataModel;
using TaskPlanner.Repository;

namespace TaskPlanner.Validaton
{
    public class CheckDublicates
    {
        /// <summary>
        /// check dublicate Email on create new user
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userRepository"></param>
        /// <param name="email"></param>
        /// <returns>true - valid user Email; false - invalid</returns>
        public bool NewUserEmailCheck(PlannerBaseEntities context, UserRepository userRepository, string email)
        {
            email=email.ToLower();
            return !userRepository.Get(context, u => u.Email == email).Any();
        }
        /// <summary>
        /// check dublicate Email on change user
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userRepository"></param>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        /// <returns>true - valid user Email; false - invalid</returns>
        public bool ChangeUserEmailCheck(PlannerBaseEntities context,UserRepository userRepository, string email, Guid userId)
        {
            email=email.ToLower();
            return !userRepository.Get(context, u => u.Email == email && u.Id != userId).Any();
        }

    }
}