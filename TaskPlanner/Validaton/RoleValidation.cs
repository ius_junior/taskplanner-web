﻿using System;
using System.Linq;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;
namespace TaskPlanner.Validaton
{


    public class RoleValidation
    {
        public Constants.Role GetRoleByTeamId(Guid userId, Guid teamId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetRoleByTeamId(context, userId, teamId);
            }
        }
        public Constants.Role GetRoleByTeamId(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            var user = userToTeamRepository.Get(context, u => u.UserId == userId && u.TeamId == teamId).FirstOrDefault();
            if (user != null)
                return (Constants.Role)user.Role;

            else
                return Constants.Role.Guest;
        }

        public Constants.Role GetRoleByProjectId(Guid userId, Guid projectId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetRoleByProjectId(context, userId, projectId);
            }
        }
        public Constants.Role GetRoleByProjectId(PlannerBaseEntities context, Guid userId, Guid projectId)
        {
            var projectRepository = new ProjectRepository();

            var project = projectRepository.Get(context, p => p.Id == projectId).FirstOrDefault();
            if (project != null)
            {
                return GetRoleByTeamId(context, userId, project.TeamId);
            }
            return Constants.Role.Guest;
        }

        public Constants.Role GetRoleByTaskId(PlannerBaseEntities context,Guid userId, Guid taskId)
        {
            var taskRepository = new TaskRepository();
            var task = taskRepository.Get(context, t => t.Id == taskId).FirstOrDefault();
            if (task != null)
            {
                return GetRoleByProjectId(context, userId, task.ProjectId);
            }
            return Constants.Role.Guest;

        }
        public Constants.Role GetRoleByTaskId(Guid userId,Guid taskId)
        {
            using (var context = new PlannerBaseEntities())
            {
                return GetRoleByTaskId(context, userId, taskId);
            }
        }



    }
}