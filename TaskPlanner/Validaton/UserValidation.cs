﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskPlanner.EntityDataModel;
using TaskPlanner.ProjectConstants;
using TaskPlanner.Repository;

namespace TaskPlanner.Validaton
{
    public class UserValidation
    {
        public bool IsUserInTeam(PlannerBaseEntities context, Guid userId, Guid teamId)
        {
            var userToTeamRepository = new UserToTeamRepository();
            return userToTeamRepository.Get(context, u => u.UserId == userId && u.TeamId == teamId && (u.Role == (byte)Constants.Role.User || u.Role == (byte)Constants.Role.Admin)).Any();
        }
        public bool IsUserInProject(PlannerBaseEntities context, Guid userId, Guid projectId)
        {
            var projectRepository = new ProjectRepository();
            var project = projectRepository.Get(context, p => p.Id == projectId).FirstOrDefault();
            if (project != null)
                return IsUserInTeam(context, userId, project.TeamId);
            return false;
        }
    }
}