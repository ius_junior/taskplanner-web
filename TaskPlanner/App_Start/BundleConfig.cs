﻿using System.Web;
using System.Web.Optimization;

namespace TaskPlanner
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap*",
                      "~/Scripts/respond.js",
                      "~/Scripts/RegisterValidation.js",
                      "~/Scripts/AuthValidation.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap*",
                      "~/Content/Site.css",
                      "~/Content/loading-bar.css",
                      "~/Content/datepicker.css"));

            //"~/Scripts/angular-ui/ui-bootstrap-tpls.js"
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/i18n/angular-locale_ru-ru.js",
                      "~/Scripts/angular-cookies.js",
                      "~/Scripts/angular-ui-router.js",
                      "~/Scripts/angular-file-upload.js",
                      "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                      "~/Scripts/angular-animate.js",
                      "~/Scripts/angular-strap.js",
                      "~/Scripts/angular-strap.tpl.js",
                      "~/Scripts/loading-bar.js",
                      "~/Scripts/Base64.js",
                      "~/Scripts/angular-charts.min.js",
                      "~/Scripts/d3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/Factories/team.js",
                      "~/Scripts/app/Factories/project.js",
                      "~/Scripts/app/Factories/user.js",
                      "~/Scripts/app/Factories/task.js",
                      "~/Scripts/app/Factories/invite.js",
                      "~/Scripts/app/Factories/comment.js",
                      "~/Scripts/app/Ctrl/login.js",
                      "~/Scripts/app/Ctrl/user.js",
                      "~/Scripts/app/Ctrl/register.js",
                      "~/Scripts/app/Ctrl/page.js",
                      "~/Scripts/app/Ctrl/team.js",
                      "~/Scripts/app/Ctrl/project.js",
                      "~/Scripts/app/Ctrl/task.js",
                      "~/Scripts/app/Ctrl/statistic.js",
                      "~/Scripts/app/Ctrl/teamStatistic.js"));
        }
    }
}
